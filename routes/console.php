<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('listen', function () {
    Redis::psubscribe(['*'], function ($message, $channel) {
        echo $channel;
    });
})->describe('Display an inspiring quote');

Artisan::command('inspire', function () {
    $user = User::all()->first();
    dd($user->modems);
})->describe('Display an inspiring quote');


