<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::any('{a?}', 'AngularController@index');
Route::any('{a?}/{b?}', 'AngularController@index');
Route::any('{a?}/{b?}/{c?}', 'AngularController@index');
Route::any('{a?}/{b?}/{c?}/{d?}', 'AngularController@index');
