<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        \App\User::create([
            'first_name' => 'test',
            'last_name' => 'te',
            'phone' => '0766920642',
            'verified' => true,
            'email' => 'gabi.brosteanu@gmail.com',
            'password' => bcrypt('admin'),
            'token' => str_random(230),
            'username' => 'gabi91cata'
        ]);

        \App\Operator::create([
            'mcc' => '22610',
            'name' => 'Orange Romania'
        ]);

        \App\Operator::create([
            'mcc' => '22601',
            'name' => 'Vodafone Romania'
        ]);
        \App\Operator::create([
            'mcc' => '22603',
            'name' => 'Telekom Romania'
        ]);
        \App\Operator::create([
            'mcc' => '22605',
            'name' => 'Digi.Mobil Romania'
        ]);
    }
}
