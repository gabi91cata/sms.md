'use strict';

angular.module('yaru22.angular-timeago').config(function(timeAgoSettings) {
  timeAgoSettings.strings['ro_RO'] = {
    prefixAgo: 'acum',
    prefixFromNow: 'peste',
    suffixAgo: '',
    suffixFromNow: 'peste',
    seconds: 'cateva secunde',
    minute: 'un minut',
    minutes: '%d minute',
    hour: 'o ora',
    hours: '%d ore',
    day: 'o zi',
    days: '%d zile',
    month: 'o luna',
    months: '%d luni',
    year: 'un an',
    years: '%d ani',
    numbers: []
  };
});
