/*
	angular-aviary v0.6.0
	(c) 2016 Massimiliano Sartoretto <massimilianosartoretto@gmail.com>
	License: MIT
*/

/* global define */


(function () {
  'use strict';

  function ngAviary(angular, Aviary) {




    ngAviaryDirective.$inject = ['ngAviary'];
    var featherEditor;

    function ngAviaryDirective(ngAviary) {
      return {
        restrict: 'A',
        scope: {
          targetSelector: '@',
          targetSrc: '@',
          onSave: '&',
          ngModel: '=',
          forceCrop: '@',
          cropMessage: '@',
          onSaveButtonClicked: '&',
          onClose:  '&'
        },
        link: function (scope, element, attrs) {
          var targetImage = window.document.querySelector(scope.targetSelector);

          var timestamp = attrs.ngModel+'_image'+(new Date()).getTime();


          element.bind('click', function(e) {
            e.preventDefault();
            $("#"+timestamp).remove();
            $('body').append('<img class="image-editor" src="'+scope.ngModel+'" id="'+timestamp+'">');

            var options = {
              image: timestamp,
            //  url: scope.ngModel,
              cropPresets: [
                ['Profil','1:1'],

              ],
              cropPresetsStrict:true,
              onSave: function  (imageID, newURL) {

                var oldURL = scope.ngModel;
                scope.ngModel = newURL;
                scope.$apply();
             //   featherEditor.close();
               scope.onSave({
                 $old:oldURL,
                 $new:newURL
               });


                if(scope.closeOnSave || ngAviary.configuration.closeOnSave){

                }
              }
            };

            if(scope.forceCrop)
            {
              options.forceCropPreset =  ['Imagine',scope.forceCrop];
              options.forceCropMessage =  scope.cropMessage;

            }

            featherEditor.launch(options);

          });

          // Callbacks obj
          var cbs = {
            onSaveButtonClicked: onSaveButtonClickedCb,

            onError: onErrorCb,
            onClose: onCloseCb
          };
          if(!featherEditor)
          {

            featherEditor = window.featherEditor = new Aviary.Feather(
                angular.extend({}, ngAviary.configuration, cbs)
            );

          }
          function onSaveButtonClickedCb(imageID) {
            // User onSaveButtonClicked callback
            (scope.onSaveButtonClicked || angular.noop)({id: imageID});
          }



          function onErrorCb(errorObj) {
            // User errback
            (scope.onError || angular.noop)({
              error: errorObj
            });
          }

          function onCloseCb(isDirty) {
            // User onClose callback
            (scope.onClose || angular.noop)({
              isDirty: isDirty
            });
          }
        }
      };
    }

    function ngAviaryProvider(){
      /* jshint validthis:true */

      var defaults = {
        apiKey: null
      };

      var requiredKeys = [
        'apiKey'
      ];

      var config;

      this.configure = function(params) {
        // Can only be configured once
        if (config) {
          throw new Error('Already configured.');
        }

        // Check if it is an `object`
        if (!(params instanceof Object)) {
          throw new TypeError('Invalid argument: `config` must be an `Object`.');
        }

        // Extend default configuration
        config = angular.extend({}, defaults, params);

        // Check if all required keys are set
        angular.forEach(requiredKeys, function(key) {
          if (!config[key]) {
            throw new Error('Missing parameter:', key);
          }
        });

        return config;
      };

      this.$get = function() {
        if(!config) {
          throw new Error('ngAviary must be configured first.');
        }

        var getConfig = (function() {
          return config;
        })();

        return {
          configuration: getConfig
        };
      };
    }

    return angular
      .module('ngAviary', [])
      .directive('ngAviary', ngAviaryDirective)
      .provider('ngAviary', ngAviaryProvider);
  }

  if (typeof define === 'function' && define.amd) {
		define(['angular', 'Aviary'], ngAviary);
	} else if (typeof module !== 'undefined' && module && module.exports) {
		ngAviary(angular, require('Aviary'));
		module.exports = 'ngAviary';
	} else {
		ngAviary(angular, (typeof global !== 'undefined' ? global : window).Aviary);
	}
})();
