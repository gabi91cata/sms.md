/*global window,location*/
(function (window) {
  'use strict';

  var Simditor = window.Simditor;
  var directives = angular.module('simditor',[]);

  directives.directive('simditor', function () {
    return {
      require: "?^ngModel",
      link: function (scope, element, attrs, ngModel) {
        element.append("<div style='height:300px;'></div>");
        var TOOLBAR_DEFAULT = ['title', 'bold', 'italic', 'underline', 'strikethrough', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|', 'link', 'image', 'hr', '|', 'indent', 'outdent'];

        var toolbar = scope.$eval(attrs.toolbar) || TOOLBAR_DEFAULT;
        scope.simditor = new Simditor({
          textarea: element.children()[0],
          placeholder:attrs.placeholder,
          pasteImage: true,
          toolbar: toolbar,
          defaultImage: 'assets/images/image.png',
          upload: location.search === '?upload' ? {
            url: '/upload'
          } : false
        });
        scope.simditor.focus();

        function readViewText() {
          var html = $(element).find('.simditor-body').html();
          if (attrs.stripBr && html === '<br>') {
            html = '';
          }
          ngModel.$setViewValue(html);
        }

        var $target = $(element).find('.simditor-body');

        ngModel.$render = function () {
          scope.simditor.focus();
          $(element).find('.simditor-body').click(function(){

            scope.simditor.blur();
            scope.simditor.focus();

          });

          $target.prepend(ngModel.$viewValue);
        };

        scope.simditor.on('valuechanged', function(){
          scope.$apply(readViewText);
        });
      }
    };
  });
}(window));