var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/**
 * Created by MacBook on 13.02.2016.
 */
var Angular;
(function (Angular) {
    var Loading = (function () {
        function Loading() {
            Angular.Global.app.directive('ngLoading', function () {
                return {
                    restrict: 'A',
                    transclude: true,
                    scope: {
                        cond: '=ngLoading',
                        class: '@'
                    },
                    link: function (scope, element, attr) {
                        $(element).removeClass(scope.class);
                    },
                    template: "\n                    <div class=\"relative {{ class }}\" >\n                        <div ng-transclude></div>\n                        <div class=\"cssload-container\" ng-if=\"cond\">\n                            \n                            <md-progress-linear md-mode=\"indeterminate\"  ></md-progress-linear> \n                            </div>\n                    </div>"
                };
            });
            Angular.Global.app.directive('ngInvalid', function () {
                return {
                    restrict: 'A',
                    transclude: true,
                    scope: {
                        cond: '=ngInvalid',
                        class: '@'
                    },
                    link: function (scope, element, attr) {
                        $(element).removeClass(scope.class);
                    },
                    template: "\n                    <div class=\"relative {{ class }}\" >\n                        <div ng-transclude></div>\n                        <div class=\"cssload-container\" ng-if=\"cond\">\n                            \n                            \n                            </div>\n                    </div>"
                };
            });
        }
        return Loading;
    }());
    Angular.Loading = Loading;
})(Angular || (Angular = {}));
/**
 * Created by MacBook on 13.02.2016.
 */
var Angular;
(function (Angular) {
    var Container = (function () {
        function Container() {
            Angular.Global.app.directive('container', function () {
                return {
                    restrict: 'E',
                    scope: {
                        col: '=',
                        align: '@'
                    },
                    transclude: true,
                    template: "\n                        <div class=\"container\">\n                            <div class=\"row\">\n                                <div class=\"{{ class }}\" ng-transclude>\n                                </div>\n                            </div>\n                        </div>",
                    link: function (scope) {
                        if (scope.align == 'c' || !scope.align) {
                            scope.class = 'col-md-' + scope.col;
                            var offset = (12 - scope.col) / 2;
                            scope.class += ' col-md-offset-' + offset;
                        }
                        if (scope.align == 'r') {
                            scope.class = 'col-md-' + scope.col;
                            var offset = (12 - scope.col);
                            scope.class += ' col-md-offset-' + offset;
                        }
                        if (scope.align == 'l') {
                            scope.class = 'col-md-' + scope.col;
                        }
                    }
                };
            });
        }
        return Container;
    }());
    Angular.Container = Container;
})(Angular || (Angular = {}));
/**
 * Created by MacBook on 13.02.2016.
 */
var Angular;
(function (Angular) {
    var Pagination = (function () {
        function Pagination() {
            Angular.Global.app.directive('ngPagination', function () {
                return {
                    restrict: 'E',
                    scope: {
                        callback: '=ngCallback',
                        data: '=ngData'
                    },
                    template: "\n                    <ul class=\"pagination\" ng-if=\"data.pages.length > 1\">\n                        <li><a ng-click=\"callback(1)\" href=\"\">\u00AB</a></li>\n                        <li ng-repeat=\"page in data.pages\" ng-class=\"{active:page.active}\"><a ng-click=\"callback(page.page)\" href=\"\" ng-bind=\"page.page\"></a></li>\n                        <li><a ng-click=\"callback(data.last_page)\" href=\"\">\u00BB</a></li>\n                    </ul>\n                    "
                };
            });
        }
        return Pagination;
    }());
    Angular.Pagination = Pagination;
})(Angular || (Angular = {}));
/**
 * Created by MacBook on 13.02.2016.
 */
var Angular;
(function (Angular) {
    var Input = (function () {
        function Input() {
            Angular.Global.app.directive('ngInput', function () {
                return {
                    restrict: 'E',
                    scope: {
                        type: '@',
                        name: '@',
                        label: '@',
                        ngModel: '='
                    },
                    transclude: true,
                    template: "\n                    <div class=\"form-group label-floating\" ng-transclude=\"\">\n                        <label class=\"control-label\" for=\"{{name}}\">{{label}}</label>\n                        <input class=\"form-control\" id=\"{{name}}\" type=\"{{type}}\" autocomplete=\"false\"  ng-model=\"ngModel\" >\n                    </div>\n                    "
                };
            });
        }
        return Input;
    }());
    Angular.Input = Input;
})(Angular || (Angular = {}));
/**
 * Created by MacBook on 13.02.2016.
 */
var Angular;
(function (Angular) {
    var Flip = (function () {
        function Flip() {
            Angular.Global.app.directive('ngFlip', function () {
                return {
                    restrict: 'A',
                    transclude: true,
                    scope: {
                        cond: '=ngFlip',
                        class: '@'
                    },
                    link: function (scope, element, attr) {
                        $(element).removeClass(scope.class);
                    },
                    template: "\n                    <div class=\"flip-container {{ class }}\" ontouchstart=\"this.classList.toggle('hover');\">\n                        <div class=\"flipper\">\n                            <div class=\"front\">\n                                 <div ng-transclude></div>\n                            </div>\n                            <div class=\"back md-whiteframe-12dp\" ng-if=\"cond\" >\n                               <div class=\"cssload-container\" >\n                            <table>\n                                <tr>\n                                    <td>\n                                     <div layout=\"row\" layout-sm=\"column\" layout-align=\"space-around\">\n    <md-progress-circular md-mode=\"indeterminate\"></md-progress-circular>\n  </div>\n\n                                    </td>\n                                    </tr>\n                                </table>\n                            </div>\n                            </div>\n                        </div>\n                    </div>"
                };
            });
        }
        return Flip;
    }());
    Angular.Flip = Flip;
})(Angular || (Angular = {}));
/**
 * Created by MacBook on 29.08.15.
 */
var Fifth;
(function (Fifth) {
    var Controller = (function () {
        function Controller() {
        }
        Controller.prototype.getCurrentURL = function () {
            return window.location.pathname;
        };
        return Controller;
    }());
    Fifth.Controller = Controller;
})(Fifth || (Fifth = {}));
///<reference path="../../app/controllers/BaseController.ts"/>
'use strict';
var Dashboard;
(function (Dashboard) {
    var Controller = Fifth.Controller;
    var ImageController = (function (_super) {
        __extends(ImageController, _super);
        function ImageController() {
            _super.apply(this, arguments);
        }
        ImageController.Profile = function ($scope, $rootScope, $timeout, $routeParams, fgDelegate, route) {
            var _this = this;
            $scope.loading = true;
            var cropper;
            var zoom = 1;
            setTimeout(function () {
                cropper = $('.image-editor').cropit({
                    exportZoom: 1.25,
                    imageBackground: true
                });
                $(".cropit-image-input").change(function () {
                    $scope.loading = false;
                    $scope.$apply();
                    $(_this).val('');
                });
                $('.rotate-cw').click(function () {
                    $('.image-editor').cropit('rotateCW');
                });
                $('.load-image').click(function () {
                    $(".cropit-image-input").trigger('click');
                    $scope.loading = true;
                    $scope.$apply();
                });
                $('.rotate-ccw').click(function () {
                    $('.image-editor').cropit('rotateCCW');
                });
                $(".cropit-image-input").trigger('click');
            }, 150);
            $scope.save = function () {
                route.model($('.image-editor').cropit('export'));
                $scope.closeThisDialog();
            };
        };
        return ImageController;
    }(Controller));
    Dashboard.ImageController = ImageController;
})(Dashboard || (Dashboard = {}));
///<reference path="../../../modules/dashboard/ImageController.ts"/>
var Angular;
(function (Angular) {
    var ImageController = Dashboard.ImageController;
    var Dialog = (function () {
        function Dialog() {
            Angular.Global.app.service('Dialog', function ($mdDialog, ngDialog) {
                this.changeProfile = function (cb, image) {
                    if (image === void 0) { image = null; }
                    var dialog = ngDialog.open({
                        template: 'dash.image.profile',
                        showClose: false,
                        controller: ImageController.Profile,
                        resolve: {
                            route: function () {
                                return {
                                    model: cb,
                                    images: image
                                };
                            }
                        }
                    });
                };
                this.confirm = function (title, text, target, parent, ok, cancel) {
                    if (ok === void 0) { ok = "Da"; }
                    if (cancel === void 0) { cancel = "Renunta"; }
                    var confirm = $mdDialog.confirm()
                        .title(title)
                        .textContent(text)
                        .clickOutsideToClose(true)
                        .parent(angular.element($(parent)))
                        .targetEvent(target)
                        .ok(ok)
                        .cancel(cancel);
                    return $mdDialog.show(confirm);
                };
                this.alert = function (title, text, target, parent, ok) {
                    if (ok === void 0) { ok = "OK"; }
                    var confirm = $mdDialog.alert()
                        .title(title)
                        .textContent(text)
                        .clickOutsideToClose(true)
                        .parent(angular.element($(parent)))
                        .targetEvent(target)
                        .ok(ok);
                    return $mdDialog.show(confirm);
                };
                this.errors = function (errors) {
                    var messages = "";
                    console.error(errors);
                    for (var error in errors) {
                        for (var _i = 0, _a = errors[error]; _i < _a.length; _i++) {
                            var e = _a[_i];
                            messages += e + "<br/>";
                        }
                    }
                    Angular.Global.$mdToast.show({
                        hideDelay: 6000,
                        position: 'top right',
                        controller: function () { },
                        template: "<md-toast style=\"z-index: 99999999;\" class=\"error-toast\">\n  <span  >" + messages + "</span> \n</md-toast>"
                    });
                };
                this.show = function (view, controller, data, ev) {
                    return $mdDialog.show({
                        controller: controller,
                        templateUrl: view,
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        locals: data
                    });
                };
                this.swal = function (view, object) {
                    var dialog = ngDialog.open({
                        template: view,
                        showClose: false,
                        controller: function ($scope, $location) {
                            $scope.object = object;
                            $scope.open = function () {
                            };
                        }
                    });
                };
                return this;
            });
        }
        return Dialog;
    }());
    Angular.Dialog = Dialog;
})(Angular || (Angular = {}));
/**
 * Created by MacBook on 13.02.2016.
 */
var Config;
(function (Config) {
    Config.app = {
        api: 'https://adoclive.com/api/',
        ws: 'wss://sv.adoclive.com:8081'
    };
})(Config || (Config = {}));
///<reference path="../../angular/Global.ts"/>
///<reference path="../../../config/App.ts"/>
var Ajax;
(function (Ajax) {
    Ajax.Get = function (url) {
        return Angular.Global.$http.get(Config.app.api + url);
    };
    Ajax.Post = function (url, data) {
        data['_token'] = window['CSRF_TOKEN'];
        console.log(window);
        return Angular.Global.$http.post(Config.app.api + url, data);
    };
    Ajax.Delete = function (url) {
        return Angular.Global.$http.delete(Config.app.api + url);
    };
})(Ajax || (Ajax = {}));
///<reference path="../../angular/Global.ts"/>
///<reference path="../ajax/Ajax.ts"/>
///<reference path="../../../routes.ts"/>
var Authentification;
(function (Authentification) {
    var Get = Ajax.Get;
    var Post = Ajax.Post;
    var Auth = (function () {
        function Auth() {
            this.loading = false;
            this.errors = {};
            return this;
        }
        Auth.on = function (type, callback) {
            if (!Auth.cbs[type])
                Auth.cbs[type] = [];
            Auth.cbs[type].push(callback);
        };
        /**
         * Here we grab the user and see if it's logged
         * @returns {JQueryXHR|*|ICacheObject|T|IHttpPromise<T>|any}
         */
        Auth.user = function () {
            if (window['user_json'])
                return window['user_json'];
            return Angular.Global.$storage.get('auth:user');
        };
        Auth.watch = function (cb) {
            Angular.Global.$scope.$watch(function () {
                return Auth.user();
            }, cb, true);
        };
        /**
         * Here will logout the application
         */
        Auth.logout = function () {
            Get('Logout').success(function () {
                Angular.Global.$storage.set('auth:user', null);
                window['user_json'] = null;
                for (var i in Auth.cbs['logout'])
                    Auth.cbs['logout'][i]();
                Global.$location.hash('');
                Global.$location.path('/');
            });
        };
        Auth.prototype.register = function (type) {
            var _this = this;
            this['type'] = type;
            this.loading = true;
            Post('Register', this).success(function (result) {
                _this.loading = false;
                Global.$location.hash('');
                Global.$mdToast.show(Global.$mdToast.simple().position('top right').textContent("Contul a fost creat. Te rugam sa verifici si sa confirmi adresa de email."));
                Global.Dialog.swal('swals.register', result);
            }).error(function (errors) {
                Global.Dialog.errors(errors);
                _this.loading = false;
            });
        };
        /**
         * Here we submit the data to the server to verify it
         */
        Auth.prototype.submit = function () {
            var _this = this;
            this.loading = true;
            this.errors = [];
            Post('Login', this).success(function (result) {
                var user = result.user;
                if (!user) {
                    _this.errors = result.errors;
                    _this.loading = false;
                    return;
                }
                user.token = result.token;
                Angular.Global.$storage.set('auth:user', user);
                for (var i in Auth.cbs['login'])
                    Auth.cbs['login'][i](user);
                var intent = Angular.Global.$storage.get('intent');
                Angular.Global.$storage.remove('intent');
                if (intent)
                    window.location.assign(intent);
                else {
                    Global.$location.hash('');
                }
            }).error(function (errors) {
                console.log(errors);
                Global.Dialog.errors(errors);
                _this.loading = false;
            });
            ;
        };
        Auth.cbs = {};
        return Auth;
    }());
    Authentification.Auth = Auth;
})(Authentification || (Authentification = {}));
///<reference path="../../angular/Global.ts"/>
///<reference path="../authentification/Auth.ts"/>
var Socket;
(function (Socket) {
    var Auth = Authentification.Auth;
    var Websocket = (function () {
        function Websocket(localStorageService) {
            var _this = this;
            this.localStorageService = localStorageService;
            this.callbacks = {};
            this.init();
            Auth.on('login', function (user) {
                _this.init();
            });
            Auth.on('logout', function (user) {
                _this.init();
            });
            return this;
        }
        Websocket.prototype.publish = function (type, object) {
            var _this = this;
            object['type'] = type;
            if (this.ws.readyState == 1)
                this.ws.send(JSON.stringify(object));
            else
                this.ws.addEventListener('open', function () {
                    _this.ws.send(JSON.stringify(object));
                });
        };
        Websocket.prototype.onMessage = function (type, callback) {
            if (type instanceof Array) {
                for (var _i = 0, type_1 = type; _i < type_1.length; _i++) {
                    var t = type_1[_i];
                    if (!this.callbacks[t])
                        this.callbacks[t] = [];
                    this.callbacks[t].push(callback);
                }
            }
            else {
                if (!this.callbacks[type])
                    this.callbacks[type] = [];
                this.callbacks[type].push(callback);
            }
        };
        Websocket.prototype.init = function () {
            var _this = this;
            var user = this.localStorageService.get('auth:user');
            if (!user)
                user = { password: 'guest' };
            if (this.ws)
                this.ws.close();
            this.ws = new WebSocket(Config.app.ws + "?token=" + user.sip_password);
            this.ws.addEventListener('message', function (data) {
                var o = JSON.parse(data.data);
                var object = o.data;
                for (var type in _this.callbacks) {
                    if (type == object.type || type == '*') {
                        for (var j in _this.callbacks[type]) {
                            _this.callbacks[type][j](object);
                        }
                    }
                }
                Angular.Global.$scope.$apply();
            });
            this.ws.addEventListener('open', function () {
                _this.ws.send(JSON.stringify({ subscribe: 'App.User.' + user.id }));
            });
            this.ws.addEventListener('close', function () {
            });
            this.ws.addEventListener('error', function () {
            });
        };
        return Websocket;
    }());
    Socket.Websocket = Websocket;
})(Socket || (Socket = {}));
///<reference path="../../angular/Global.ts"/>
var Imager;
(function (Imager) {
    var CropImage = (function () {
        function CropImage() {
            this.init();
            return this;
        }
        CropImage.prototype.profile = function (src) {
            var _this = this;
            window['featherEditor'].launch({
                image: 'test',
                tools: 'enhance,effects,orientation,crop,lighting,color,sharpness,focus,vignette,text',
                url: 'http://consultadoctor.ro/resize.php?zc=1&h=450&w=750&src=/images/spuneNu.jpg',
                onSave: function (imageID, newURL) {
                    console.log(newURL);
                    _this.featherEditor.close();
                }
            });
        };
        CropImage.prototype.init = function () {
        };
        return CropImage;
    }());
    Imager.CropImage = CropImage;
})(Imager || (Imager = {}));
var Angular;
(function (Angular) {
    var Animation = (function () {
        function Animation() {
            this.restrict = 'A';
            this.link = function (scope, element, attrs) {
                var delay = 0;
                if (attrs.delay) {
                    delay = attrs.delay;
                    $(element).addClass('hide');
                }
                setTimeout(function () {
                    $(element).removeClass('hide');
                    $(element).addClass('animated ' + attrs.ngAnimate);
                }, delay);
            };
            return this;
        }
        return Animation;
    }());
    Angular.Animation = Animation;
})(Angular || (Angular = {}));
/**
 * Created by MacBook on 13.02.2016.
 */
var Angular;
(function (Angular) {
    var Rating = (function () {
        function Rating() {
            Angular.Global.app.directive('ngRating', function () {
                return {
                    restrict: 'E',
                    scope: {
                        stars: '=stars'
                    },
                    link: function (scope, element, attr) {
                        $(element).removeClass(scope.class);
                        scope.integer = Math.round(scope.stars);
                        scope.decimal = scope.integer - scope.stars;
                        scope.decimal = Math.round(Math.abs(scope.decimal) * 10);
                    },
                    template: "   \n                        <md-icon class=\"star md-primary\" ng-repeat=\"item in [1,2,3,4,5]\" ng-if=\"item<=stars\" >star</md-icon>\n                        <md-icon class=\"star md-primary\" ng-repeat=\"item in [1,2,3,4,5]\" ng-if=\"item>stars\" >star_border</md-icon>\n                    \n                     "
                };
            });
        }
        return Rating;
    }());
    Angular.Rating = Rating;
})(Angular || (Angular = {}));
///<reference path="directives/Loading.ts"/>
///<reference path="directives/Container.ts"/>
///<reference path="directives/Pagination.ts"/>
///<reference path="directives/Input.ts"/>
///<reference path="directives/Flip.ts"/>
///<reference path="services/Dialog.ts"/>
///<reference path="../fifth/websocket/Websocket.ts"/>
///<reference path="Global.ts"/>
///<reference path="../fifth/image/Crop.ts"/>
///<reference path="directives/Animation.ts"/>
///<reference path="directives/Rating.ts"/>
/**
 * Created by MacBook on 13.02.2016.
 */
var Angular;
(function (Angular) {
    var Websocket = Socket.Websocket;
    var Services = (function () {
        function Services() {
            ///Services
            new Angular.Dialog();
            Angular.Global.app.service('Websocket', Websocket);
            ///Directives
            new Angular.Loading();
            new Angular.Rating();
            new Angular.Flip();
            new Angular.Container();
            new Angular.Pagination();
            new Angular.Input();
            Angular.Global.app.directive('ngAnimate', Angular.Animation);
        }
        return Services;
    }());
    Angular.Services = Services;
})(Angular || (Angular = {}));
/**
 * Created by MacBook on 29.08.15.
 */
var Providers;
(function (Providers) {
    var NotificationsProvider = (function () {
        function NotificationsProvider($scope, Websocket, $mdToast, $location) {
            this.$scope = $scope;
            this.Websocket = Websocket;
            this.$mdToast = $mdToast;
            this.$location = $location;
            this.init();
            return this;
        }
        NotificationsProvider.prototype.init = function () {
            var _this = this;
            var toastShown = false;
            this.Websocket.onMessage([
                "App\\Notifications\\AppointmentReminder",
                "App\\Notifications\\AppointmentBooked",
                "App\\Notifications\\AppointmentReview",
                "App\\Notifications\\AppointmentCanceled"
            ], function (message) {
                if (toastShown) {
                    _this.$mdToast.updateTextContent(message.text);
                    return;
                }
                var toast = _this.$mdToast.simple()
                    .textContent(message.text)
                    .action('Deschide')
                    .hideDelay(16000)
                    .highlightClass('md-accent') // Accent is used by default, this just demonstrates the usage.
                    .position('top right');
                toastShown = true;
                _this.$mdToast.show(toast).then(function (response) {
                    if (response == 'ok') {
                        _this.$location.path(message.action);
                        _this.$location.search('');
                    }
                    toastShown = false;
                });
            });
        };
        return NotificationsProvider;
    }());
    Providers.NotificationsProvider = NotificationsProvider;
})(Providers || (Providers = {}));
///<reference path="../../Functions.ts"/>
///<reference path="Services.ts"/>
///<reference path="../fifth/ajax/Ajax.ts"/>
///<reference path="../fifth/authentification/Auth.ts"/>
///<reference path="services/Dialog.ts"/>
///<reference path="../../modules/dashboard/ImageController.ts"/>
///<reference path="../../app/providers/NotificationsProvider.ts"/>
var Angular;
(function (Angular) {
    var Auth = Authentification.Auth;
    var Global = (function () {
        function Global() {
        }
        Global.meta = function (title) {
            document.title = title;
        };
        Global.Load = function () {
            var c;
        };
        Global.Init = function (cb) {
            Date.prototype.toJSON = function (a) {
                var d = new Date(this.valueOf() - this.getTimezoneOffset() * 60000);
                ;
                return d.toISOString();
            };
            this.app = angular.module('fifth', ['ngRoute', 'LocalStorageModule', 'ngDialog', 'ngMaterial']);
            $("body").on("mouseover", "[md-selectable]", function (e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).parent().parent().addClass('md-non-selectable');
                console.log($(this).parent().parent());
            });
            this.app.directive('mdSelectable', function ($parse) {
                return {
                    restrict: 'A',
                    link: function ($scope, $elem, iAttrs) {
                    }
                };
            }).filter('cut', function () {
                return function (value, wordwise, max, tail) {
                    if (!value)
                        return '';
                    max = parseInt(max, 10);
                    if (!max)
                        return value;
                    if (value.length <= max)
                        return value;
                    value = value.substr(0, max);
                    if (wordwise) {
                        var lastspace = value.lastIndexOf(' ');
                        if (lastspace != -1) {
                            //Also remove . and , so its gives a cleaner result.
                            if (value.charAt(lastspace - 1) == '.' || value.charAt(lastspace - 1) == ',') {
                                lastspace = lastspace - 1;
                            }
                            value = value.substr(0, lastspace);
                        }
                    }
                    return value + (tail || ' …');
                };
            });
            this.app.filter('nl2br', function ($sce) {
                return function (msg, is_xhtml) {
                    var is_xhtml = is_xhtml || true;
                    var breakTag = (is_xhtml) ? '<br />' : '<br>';
                    var msg = (msg + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
                    msg = msg.replace(/  /g, '&nbsp; ');
                    return $sce.trustAsHtml(msg);
                };
            });
            this.app.filter('data', function ($sce) {
                return function (msg, is_xhtml) {
                    return new Date(msg);
                };
            });
            this.app.directive('endRepeat', function () {
                return {
                    controller: function ($scope) {
                        if ($scope.$last) {
                            window.alert("im the last!");
                        }
                    }
                };
            });
            this.app.filter('contains', function () {
                return function (array, needle) {
                    return array.indexOf(needle) >= 0;
                };
            });
            this.app.directive("myStream", function ($window) {
                return {
                    restrict: 'A',
                    scope: { config: '=' },
                    link: function (scope, element, attributes) {
                    }
                };
            });
            this.app.config(function ($mdThemingProvider, $sceDelegateProvider) {
                $sceDelegateProvider.resourceUrlWhitelist([
                    'self',
                    '*://www.youtube.com/**',
                    '*:blob:https://adoclive.com/**' // this might be how to test from mediaStream.
                ]);
                $mdThemingProvider.theme('default')
                    .primaryPalette('teal')
                    .accentPalette('teal');
                $mdThemingProvider.theme('black')
                    .primaryPalette('teal')
                    .accentPalette('teal');
                $mdThemingProvider.theme('200')
                    .primaryPalette('teal')
                    .accentPalette('teal');
            });
            this.app.controller('MainController', function ($scope, Websocket, Dialog, $routeParams, ngDialog, $route, $rootScope, $timeout, $location, $http, $mdToast, $q, localStorageService) {
                localStorageService.set('auth:user', window['user_json']);
                $http.defaults.headers.common['X-CSRF-TOKEN'] = window['CSRF_TOKEN'];
                $http.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                $scope.side = false;
                $rootScope.route = $routeParams;
                $scope.loaded = true;
                $rootScope.ip = window['ip'];
                Global.$http = $http;
                Global.Websocket = Websocket;
                Global.$route = $route;
                Global.$storage = localStorageService;
                Global.$q = $q;
                Global.$location = $location;
                Global.ngDialog = ngDialog;
                Global.$mdToast = $mdToast;
                Global.$scope = $scope;
                Global.Dialog = Dialog;
                $scope.logout = function () {
                    Auth.logout();
                };
                $scope.hash = function (a) {
                    $location.hash(a);
                };
                function extract(a, sp, r) {
                    if (r === void 0) { r = null; }
                    if (r == null)
                        r = [];
                    var regex = /(:[a-zA-Z0-9]+)/;
                    var b = [];
                    for (var i in sp) {
                        var item = sp[i];
                        if (item != "") {
                            if (a)
                                b = a.split(item);
                            if (b.length == 2)
                                if (b[0] == "" && b[1] == "")
                                    return true;
                            if (b[1]) {
                                //we found a match, so we continue
                                r.push(sp[1]);
                                if (sp[0] && sp[1])
                                    sp.splice(0, 2);
                                if (sp.length == 1) {
                                    var ret = {};
                                    for (var j in b) {
                                        if (b[j] == "") {
                                            b.splice(j, 1);
                                        }
                                    }
                                    if (r[0]) {
                                        for (var j in b) {
                                            ret[r[j].replace(':', '')] = b[j];
                                        }
                                    }
                                    if (!jQuery.isEmptyObject(ret))
                                        return ret;
                                }
                                return extract(b[1], sp, r);
                            }
                        }
                    }
                    return false;
                }
                $scope.$watch(function () { return $location.hash(); }, function (a) {
                    ngDialog.close();
                    if (a) {
                        var r = Route.popups[a];
                        $timeout(function () {
                            for (var route in Route.popups) {
                                var s = route.split(/(:[a-zA-Z0-9]+)/);
                                var sp = extract(a, s);
                                if (sp) {
                                    var loc = (Global.$route.current.$$route.originalPath);
                                    var r = Route.popups[route];
                                    if (r.location == loc || r.location == "*") {
                                        r.init(sp);
                                    }
                                }
                            }
                        }, 1);
                    }
                });
            });
            cb();
            this.app.directive('ngName', function ($location) {
                return {
                    scope: {
                        data: '=ngData'
                    },
                    link: function (scope, element, attrs) {
                        var route = Fifth.Route.routes[attrs.ngName];
                        for (var i in scope.data) {
                            route = route.replace(":" + i, scope.data[i]);
                        }
                        if (Global.$location.path() == route)
                            $(element).addClass('active');
                        $(element).attr('href', route);
                        if (scope.data)
                            if (scope.data.type)
                                $(element).attr('href', route + "?type=" + scope.data.type);
                        $(element).click(function () {
                            $("[ng-name]").removeClass('active');
                            $(element).addClass('active');
                            Global.$location.hash('');
                            if (!$(element).is("a")) {
                                Global.$location.path(route);
                                if (scope.data)
                                    if (!scope.data.type)
                                        Global.$location.search('');
                                    else {
                                        Global.$location.search("type", scope.data.type);
                                    }
                            }
                            console.log(scope.data);
                            Global.$scope.$apply();
                        });
                    }
                };
            });
            this.app.config(function (localStorageServiceProvider) {
                localStorageServiceProvider
                    .setPrefix('myApp')
                    .setStorageType('sessionStorage')
                    .setNotify(true, true);
            });
            new Angular.Services();
        };
        Global.vars = {};
        return Global;
    }());
    Angular.Global = Global;
})(Angular || (Angular = {}));
///<reference path="../../angular/Global.ts"/>
/**
 * Created by MacBook on 29.08.15.
 */
var Pagination;
(function (Pagination) {
    var Paginator = (function () {
        function Paginator() {
        }
        /**
         * Paginate the results
         * @returns any
         * @param val
         */
        Paginator.paginate = function (val) {
            try {
                var pages = [];
                var begin = 1;
                var end = val.last_page;
                if (val.last_page > 5) {
                    if (val.current_page > 3)
                        begin = val.current_page - 2;
                    if (val.last_page - val.current_page > 3)
                        end = val.current_page + 3;
                }
                for (var i = begin; i <= end; i++) {
                    pages.push({
                        page: i,
                        active: val.current_page == i,
                        prev: val.current_page > 1,
                        next: val.current_page < val.last_page
                    });
                }
                val.pages = pages;
            }
            catch (e) {
                console.error(e);
            }
            return val;
        };
        return Paginator;
    }());
    Pagination.Paginator = Paginator;
})(Pagination || (Pagination = {}));
///<reference path="../../angular/Global.ts"/>
///<reference path="../pagination/Pagination.ts"/>
/**
 * Created by MacBook on 29.08.15.
 */
var Models;
(function (Models) {
    var BaseModel = (function () {
        function BaseModel() {
        }
        return BaseModel;
    }());
    Models.BaseModel = BaseModel;
})(Models || (Models = {}));
/**
 * Created by Catalin on 3/11/2016.
 */
var Encoding;
(function (Encoding) {
    var Base64 = (function () {
        function Base64() {
        }
        Base64._utf8_decode = function (utftext) {
            var string = "";
            var i = 0;
            var c1, c2, c3;
            var c = c1 = c2 = 0;
            while (i < utftext.length) {
                c = utftext.charCodeAt(i);
                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                }
                else if ((c > 191) && (c < 224)) {
                    c2 = utftext.charCodeAt(i + 1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                }
                else {
                    c2 = utftext.charCodeAt(i + 1);
                    c3 = utftext.charCodeAt(i + 2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            return string;
        };
        Base64._utf8_encode = function (string) {
            var string = string.replace(/\r\n/g, "\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {
                var c = string.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                }
                else if ((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
                else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
            }
            return utftext;
        };
        Base64.encode = function (input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = Base64._utf8_encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                }
                else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output +
                    this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                    this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
            }
            return output;
        };
        Base64.decode = function (input) {
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (i < input.length) {
                enc1 = this._keyStr.indexOf(input.charAt(i++));
                enc2 = this._keyStr.indexOf(input.charAt(i++));
                enc3 = this._keyStr.indexOf(input.charAt(i++));
                enc4 = this._keyStr.indexOf(input.charAt(i++));
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
                output = output + String.fromCharCode(chr1);
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
            }
            output = Base64._utf8_decode(output);
            return output;
        };
        Base64._keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        return Base64;
    }());
    Encoding.Base64 = Base64;
})(Encoding || (Encoding = {}));
///<reference path="../../angular/Global.ts"/>
///<reference path="../pagination/Pagination.ts"/>
///<reference path="BaseModel.ts"/>
///<reference path="../encoding/Base64.ts"/>
/**
 * Created by MacBook on 29.08.15.
 */
var Models;
(function (Models) {
    var Paginator = Pagination.Paginator;
    var Base64 = Encoding.Base64;
    var Model = (function (_super) {
        __extends(Model, _super);
        function Model(attributes) {
            if (attributes === void 0) { attributes = {}; }
            _super.call(this);
            this.$class = Model;
            this.$dates = [];
            this.$with = {};
            this.$errors = {};
            this.$cache = false;
            this.saving = false;
            this.deleting = false;
            this.confirming = false;
            this.deleted = false;
            this.loading = false;
            this.attributes = {};
            this.wheres = [];
            this.listAll = [];
            this.data = {};
            this._thens = [];
            this.attributes = attributes;
            for (var i in attributes) {
                this[i] = attributes[i];
            }
            delete (this.attributes);
            return this;
        }
        Model.prototype.getDeletedText = function () {
            return "Datele au fost sterse cu succes!";
        };
        Model.prototype.getSavedText = function () {
            return "Datele au fost salvate cu succes!";
        };
        Model.prototype.getDenyText = function () {
            return "Nu ai drepturi necesare!";
        };
        /**
         * Get the model by id
         * @param id
         * @returns {Models.Model}
         */
        Model.prototype.find = function (id) {
            var _this = this;
            var where = "";
            if (this.data)
                where += '&data=' + Base64.encode(JSON.stringify(this.data));
            this.loading = true;
            Global.$http.get('/api/' + this.$table + '/' + id + "?" + where).success(function (val) {
                _this.loading = false;
                _this.constructor(val);
                _this.resolveDates();
                if (_this._then)
                    _this._then(_this);
                for (var _i = 0, _a = _this._thens; _i < _a.length; _i++) {
                    var l = _a[_i];
                    l.cb(val);
                }
            }).error(function (e) {
                console.log(e);
                _this.loading = false;
            });
            return this;
        };
        Model.prototype.later = function (val) {
            this._then = val;
            return this;
        };
        Model.prototype.laters = function (val) {
            this._thens.push({ cb: val });
            ;
            return this;
        };
        Model.prototype.reload = function (attributes) {
            this.attributes = attributes;
            for (var i in attributes) {
                this[i] = attributes[i];
            }
            delete (this.attributes);
            this.resolveDates();
        };
        /**
         * Delete current model
         * @returns {IHttpPromise<T>}
         */
        Model.prototype.remove = function ($deleteText) {
            var _this = this;
            if ($deleteText === void 0) { $deleteText = null; }
            if ($deleteText) {
                this.deleting = true;
                Global.Dialog.confirm("Confirma stergerea", $deleteText, null, null).then(function () {
                    Global.$http.delete('/api/' + _this.$table + '/' + _this['id']).success(function (val) {
                        _this.clearCache();
                        Global.$mdToast.show(Global.$mdToast.simple().textContent(_this.getDeletedText()));
                        _this.constructor(val);
                        _this.resolveDates();
                        _this.deleting = false;
                        _this.deleted = true;
                    }).error(function (errors, a) {
                        _this.deleted = false;
                        _this.deleting = false;
                        if (a == 403) {
                            Global.Dialog.errors({ email: [_this.getDenyText()] }, 10000);
                        }
                        else {
                            Global.Dialog.errors(errors, 10000);
                        }
                    });
                    ;
                }, function () {
                    _this.deleting = false;
                    _this.deleted = false;
                });
            }
            else {
                this.deleting = true;
                var d = Global.$q.defer();
                Global.$http.delete('/api/' + this.$table + '/' + this['id']).success(function (val) {
                    _this.clearCache();
                    d.resolve();
                    _this.deleting = false;
                    _this.deleted = true;
                });
                return d.promise;
            }
        };
        Model.prototype.getAll = function () {
            return this.listAll;
        };
        Model.prototype.save = function ($text) {
            var _this = this;
            if ($text === void 0) { $text = null; }
            if (this['id'] && this['id'] != 'new')
                return this.update($text);
            var d = Global.$q.defer();
            this.saving = true;
            this['_token'] = window['CSRF_TOKEN'];
            var where = "";
            if (this.data)
                where += '&data=' + Base64.encode(JSON.stringify(this.data));
            Global.$http.post('/api/' + this.$table + "?" + where, this).success(function (val) {
                _this.clearCache();
                d.resolve((new _this.$class(val)).resolveDates());
                _this.reload(val);
                _this.saving = false;
                Global.$mdToast.show(Global.$mdToast.simple().position('bottom right').textContent($text ? $text : _this.getSavedText()));
            }).error(function (errors, a) {
                _this.saving = false;
                d.reject(errors);
                _this.$errors = errors;
                if (a == 403) {
                    Global.Dialog.errors({ email: [_this.getDenyText()] }, 10000);
                }
                else {
                    Global.Dialog.errors(errors, 10000);
                }
            });
            return d.promise;
        };
        Model.prototype.update = function ($text) {
            var _this = this;
            if (!this['id'] || this['id'] == 'new')
                return this.save($text);
            var d = Global.$q.defer();
            this.saving = true;
            var where = "";
            if (this.data)
                where += '&data=' + Base64.encode(JSON.stringify(this.data));
            Global.$http.put('/api/' + this.$table + '/' + this['id'] + "?" + where, this).success(function (val) {
                _this.clearCache();
                _this.constructor(val);
                d.resolve(new _this.$class(val).resolveDates());
                _this.saving = false;
                Global.$mdToast.show(Global.$mdToast.simple().position('bottom right').textContent($text ? $text : _this.getSavedText()));
            }).error(function (errors, a) {
                _this.saving = false;
                d.reject(errors);
                _this.$errors = errors;
                if (a == 403) {
                    Global.Dialog.errors({ email: [_this.getDenyText()] }, 10000);
                }
                else {
                    Global.Dialog.errors(errors, 10000);
                }
            });
            return d.promise;
        };
        Model.prototype.with = function (data) {
            this.data = data;
            return this;
        };
        Model.prototype.where = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i - 0] = arguments[_i];
            }
            switch (args.length) {
                case 1:
                    throw Error('Must have at least 2 arguments');
                case 2:
                    this.wheres.push({
                        key: args[0],
                        condition: '=',
                        value: args[1]
                    });
                    break;
                case 3:
                    this.wheres.push({
                        key: args[0],
                        condition: args[1],
                        value: args[2]
                    });
                    break;
            }
            return this;
        };
        Model.prototype.getCache = function (name) {
            if (!this.$cache)
                return false;
            var storage = Global.$storage;
            name = Base64.encode(name);
            var n = storage.get(this.$table + ':cache:' + name);
            if (n)
                return true;
            return false;
        };
        Model.prototype.setCache = function (name, val) {
            if (!this.$cache)
                return false;
            var storage = Global.$storage;
            name = Base64.encode(name);
            var n = storage.set(this.$table + ':cache:' + name, val);
        };
        Model.prototype.caching = function (name) {
            var storage = Global.$storage;
            name = Base64.encode(name);
            var n = storage.get(this.$table + ':cache:' + name);
            if (n.data) {
                for (var i in n.data) {
                    n.data[i] = (new this.$class(n.data[i])).resolveDates();
                }
            }
            var d = Global.$q.defer();
            d.resolve(n);
            return d.promise;
        };
        Model.prototype.clearCache = function () {
            var keys = (Global.$storage.keys());
            for (var i in keys) {
                if (i.indexOf(this.$table + ':cache:') != -1)
                    Global.$storage.remove(i);
            }
        };
        /**
         * Get all the models here
         * @param page
         * @returns {IPromise<T>}
         */
        Model.prototype.get = function (page) {
            var _this = this;
            if (page === void 0) { page = null; }
            var storage = Global.$storage;
            if (!page)
                page = storage.get(this.$table + ':page');
            storage.set(this.$table + ':page', page);
            var d = Global.$q.defer();
            if (page != null)
                page = 'page=' + page;
            else
                page = '';
            var where = '';
            this.loading = true;
            if (this.wheres.length > 0)
                where = '&where=' + Base64.encode(JSON.stringify(this.wheres));
            if (this.data)
                where += '&data=' + Base64.encode(JSON.stringify(this.data));
            if (this.getCache(page + where)) {
                return this.caching(page + where);
            }
            Global.$http.get('/api/' + this.$table + '?' + page + where).then(function (response) {
                var val = Paginator.paginate(response.data);
                var loop = [];
                if (val.data)
                    loop = val.data;
                else
                    loop = val;
                if (loop[0]) {
                    for (var i in loop)
                        loop[i] = (new _this.$class(loop[i])).resolveDates();
                }
                for (var j in _this.$with) {
                    if (val[j] && !val[j].data) {
                        if (Object.prototype.toString.call(val[j]) === '[object Array]')
                            for (var i in val[j])
                                val[j][i] = (new _this.$with[j](val[j][i])).resolveDates();
                        else
                            val[j] = (new _this.$with[j](val[j])).resolveDates();
                    }
                    if (val[j] && val[j].data) {
                        if (Object.prototype.toString.call(val[j].data) === '[object Array]')
                            for (var i in val[j].data)
                                val[j].data[i] = (new _this.$with[j](val[j].data[i])).resolveDates();
                    }
                }
                _this.listAll = val;
                _this.setCache(page + where, val);
                if (val.data) {
                    val.data = loop;
                    d.resolve(val);
                }
                else
                    d.resolve(loop);
                _this.loading = false;
            });
            return d.promise;
        };
        Model.prototype.resolveDates = function () {
            var dates = this.$dates;
            dates.push('created_at');
            dates.push('updated_at');
            for (var i in this) {
                if (dates.indexOf(i) != -1) {
                    if (this[i])
                        this[i] = new Date(this[i]);
                }
            }
            for (var j in this.$with) {
                if (this[j]) {
                    if (Object.prototype.toString.call(this[j]) === '[object Array]')
                        for (var i in this[j])
                            this[j][i] = (new this.$with[j](this[j][i])).resolveDates();
                    else
                        this[j] = (new this.$with[j](this[j])).resolveDates();
                }
            }
            return this;
        };
        return Model;
    }(Models.BaseModel));
    Models.Model = Model;
})(Models || (Models = {}));
///<reference path="../vendor/fifth/models/Model.ts"/>
/**
 * Created by MacBook on 29.08.15.
 */
var Fifth;
(function (Fifth) {
    var Model = Models.Model;
    var User = (function (_super) {
        __extends(User, _super);
        function User() {
            _super.apply(this, arguments);
            this.$table = "user";
            this.$class = User;
        }
        User.prototype.select = function () {
            this.selected = true;
            Global.$location.hash('account:' + this.id);
        };
        User.prototype.cancel = function () {
            this.selected = false;
            if (this.id == "new")
                this.deleted = true;
            Global.$location.hash('');
        };
        User.prototype.update = function ($text) {
            var _this = this;
            return _super.prototype.update.call(this, $text).then(function () {
                _this.selected = false;
                Global.$location.hash('');
            });
        };
        return User;
    }(Model));
    Fifth.User = User;
})(Fifth || (Fifth = {}));
///<reference path="BaseController.ts"/>
///<reference path="../User.ts"/>
///<reference path="../../vendor/angular/Global.ts"/>
///<reference path="../../vendor/fifth/authentification/Auth.ts"/>
'use strict';
var Fifth;
(function (Fifth) {
    var Auth = Authentification.Auth;
    var AuthController = (function (_super) {
        __extends(AuthController, _super);
        function AuthController() {
            _super.apply(this, arguments);
        }
        AuthController.Login = function ($scope, $timeout) {
            $scope.login = new Auth();
        };
        AuthController.Register = function ($scope, $timeout, $location) {
            $scope.register = new Auth();
            $scope.hash = function (url) {
                $location.hash(url);
            };
        };
        AuthController.Logout = function ($scope, $timeout) {
            Auth.logout();
        };
        AuthController.Recover = function ($scope) {
        };
        return AuthController;
    }(Fifth.Controller));
    Fifth.AuthController = AuthController;
})(Fifth || (Fifth = {}));
///<reference path="vendor/angular/Global.ts"/>
///<reference path="app/controllers/AuthController.ts"/>
var Fifth;
(function (Fifth) {
    var Route = (function () {
        function Route(location) {
            this.location = location;
            return this;
        }
        Route.Url = function (name, params) {
            var route = this.routes[name];
            var d = route;
            for (var i in params) {
                d = d.replace(":" + i, params[i]);
            }
            return d;
        };
        Route.Call = function (url) {
            this.popups[url].init();
        };
        Route.Popup = function (url, options) {
            Route.popups[url] = {
                location: "*",
                init: function (a) {
                    var dialog = Angular.Global.ngDialog.open({
                        template: options.view,
                        showClose: false,
                        closeByDocument: false,
                        controller: options.uses,
                        resolve: {
                            route: function () {
                                return a;
                            }
                        }
                    });
                    dialog.closePromise.then(function (data) {
                        var s = url.split(/(:[a-zA-Z0-9]+)/);
                        var u = Global.$location.hash();
                        console.log(u, url);
                        if (u == url) {
                            Global.$location.hash('');
                        }
                        if (u)
                            return;
                        if (u.indexOf(s[0]) != -1) {
                            Global.$location.hash('');
                        }
                    });
                }
            };
            Route.routes["#" + options.as] = "#" + url;
        };
        Route.prototype.Popup = function (url, options) {
            Route.popups[url] = {
                location: this.location,
                init: function (a) {
                    var dialog = Angular.Global.ngDialog.open({
                        template: options.view,
                        showClose: false,
                        closeByDocument: false,
                        controller: options.uses,
                        resolve: {
                            route: function () {
                                return a;
                            }
                        }
                    });
                    dialog.closePromise.then(function (data) {
                        var s = url.split(/(:[a-zA-Z0-9]+)/);
                        var u = Global.$location.hash();
                        console.log(u, s[0], u.indexOf(s[0]));
                        if (u)
                            if (u.indexOf(s[0]) != -1) {
                                if (s[0] != "appointment:")
                                    Global.$location.hash('');
                            }
                    });
                }
            };
            Route.routes["#" + options.as] = this.location + "#" + url;
        };
        Route.Get = function (url, options) {
            Angular.Global.app.config(function ($routeProvider, $locationProvider) {
                $routeProvider.when(url, {
                    templateUrl: options.view,
                    controller: options.uses,
                    reloadOnSearch: options.reloadOnSearch ? options.reloadOnSearch : false,
                    redirectTo: function (a, b, c) {
                        if (options.middleware) {
                            var toReturn;
                            for (var i in options.middleware) {
                                toReturn = options.middleware[i].handle(window.location.pathname);
                                if (toReturn)
                                    return toReturn;
                            }
                        }
                    }
                });
                $locationProvider.html5Mode(true);
                $locationProvider.hashPrefix('!');
            });
            if (options.popups) {
                options.popups(new Route(url));
            }
            this.routes[options.as] = url;
        };
        Route.popups = {};
        Route.routes = {};
        return Route;
    }());
    Fifth.Route = Route;
})(Fifth || (Fifth = {}));
/*
 * JavaScript MD5
 * https://github.com/blueimp/JavaScript-MD5
 *
 * Copyright 2011, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 *
 * Based on
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.2 Copyright (C) Paul Johnston 1999 - 2009
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */
/*global unescape, define, module */
;
(function ($) {
    'use strict';
    /*
     * Add integers, wrapping at 2^32. This uses 16-bit operations internally
     * to work around bugs in some JS interpreters.
     */
    function safe_add(x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }
    /*
     * Bitwise rotate a 32-bit number to the left.
     */
    function bit_rol(num, cnt) {
        return (num << cnt) | (num >>> (32 - cnt));
    }
    /*
     * These functions implement the four basic operations the algorithm uses.
     */
    function md5_cmn(q, a, b, x, s, t) {
        return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s), b);
    }
    function md5_ff(a, b, c, d, x, s, t) {
        return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
    }
    function md5_gg(a, b, c, d, x, s, t) {
        return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
    }
    function md5_hh(a, b, c, d, x, s, t) {
        return md5_cmn(b ^ c ^ d, a, b, x, s, t);
    }
    function md5_ii(a, b, c, d, x, s, t) {
        return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
    }
    /*
     * Calculate the MD5 of an array of little-endian words, and a bit length.
     */
    function binl_md5(x, len) {
        /* append padding */
        x[len >> 5] |= 0x80 << (len % 32);
        x[(((len + 64) >>> 9) << 4) + 14] = len;
        var i;
        var olda;
        var oldb;
        var oldc;
        var oldd;
        var a = 1732584193;
        var b = -271733879;
        var c = -1732584194;
        var d = 271733878;
        for (i = 0; i < x.length; i += 16) {
            olda = a;
            oldb = b;
            oldc = c;
            oldd = d;
            a = md5_ff(a, b, c, d, x[i], 7, -680876936);
            d = md5_ff(d, a, b, c, x[i + 1], 12, -389564586);
            c = md5_ff(c, d, a, b, x[i + 2], 17, 606105819);
            b = md5_ff(b, c, d, a, x[i + 3], 22, -1044525330);
            a = md5_ff(a, b, c, d, x[i + 4], 7, -176418897);
            d = md5_ff(d, a, b, c, x[i + 5], 12, 1200080426);
            c = md5_ff(c, d, a, b, x[i + 6], 17, -1473231341);
            b = md5_ff(b, c, d, a, x[i + 7], 22, -45705983);
            a = md5_ff(a, b, c, d, x[i + 8], 7, 1770035416);
            d = md5_ff(d, a, b, c, x[i + 9], 12, -1958414417);
            c = md5_ff(c, d, a, b, x[i + 10], 17, -42063);
            b = md5_ff(b, c, d, a, x[i + 11], 22, -1990404162);
            a = md5_ff(a, b, c, d, x[i + 12], 7, 1804603682);
            d = md5_ff(d, a, b, c, x[i + 13], 12, -40341101);
            c = md5_ff(c, d, a, b, x[i + 14], 17, -1502002290);
            b = md5_ff(b, c, d, a, x[i + 15], 22, 1236535329);
            a = md5_gg(a, b, c, d, x[i + 1], 5, -165796510);
            d = md5_gg(d, a, b, c, x[i + 6], 9, -1069501632);
            c = md5_gg(c, d, a, b, x[i + 11], 14, 643717713);
            b = md5_gg(b, c, d, a, x[i], 20, -373897302);
            a = md5_gg(a, b, c, d, x[i + 5], 5, -701558691);
            d = md5_gg(d, a, b, c, x[i + 10], 9, 38016083);
            c = md5_gg(c, d, a, b, x[i + 15], 14, -660478335);
            b = md5_gg(b, c, d, a, x[i + 4], 20, -405537848);
            a = md5_gg(a, b, c, d, x[i + 9], 5, 568446438);
            d = md5_gg(d, a, b, c, x[i + 14], 9, -1019803690);
            c = md5_gg(c, d, a, b, x[i + 3], 14, -187363961);
            b = md5_gg(b, c, d, a, x[i + 8], 20, 1163531501);
            a = md5_gg(a, b, c, d, x[i + 13], 5, -1444681467);
            d = md5_gg(d, a, b, c, x[i + 2], 9, -51403784);
            c = md5_gg(c, d, a, b, x[i + 7], 14, 1735328473);
            b = md5_gg(b, c, d, a, x[i + 12], 20, -1926607734);
            a = md5_hh(a, b, c, d, x[i + 5], 4, -378558);
            d = md5_hh(d, a, b, c, x[i + 8], 11, -2022574463);
            c = md5_hh(c, d, a, b, x[i + 11], 16, 1839030562);
            b = md5_hh(b, c, d, a, x[i + 14], 23, -35309556);
            a = md5_hh(a, b, c, d, x[i + 1], 4, -1530992060);
            d = md5_hh(d, a, b, c, x[i + 4], 11, 1272893353);
            c = md5_hh(c, d, a, b, x[i + 7], 16, -155497632);
            b = md5_hh(b, c, d, a, x[i + 10], 23, -1094730640);
            a = md5_hh(a, b, c, d, x[i + 13], 4, 681279174);
            d = md5_hh(d, a, b, c, x[i], 11, -358537222);
            c = md5_hh(c, d, a, b, x[i + 3], 16, -722521979);
            b = md5_hh(b, c, d, a, x[i + 6], 23, 76029189);
            a = md5_hh(a, b, c, d, x[i + 9], 4, -640364487);
            d = md5_hh(d, a, b, c, x[i + 12], 11, -421815835);
            c = md5_hh(c, d, a, b, x[i + 15], 16, 530742520);
            b = md5_hh(b, c, d, a, x[i + 2], 23, -995338651);
            a = md5_ii(a, b, c, d, x[i], 6, -198630844);
            d = md5_ii(d, a, b, c, x[i + 7], 10, 1126891415);
            c = md5_ii(c, d, a, b, x[i + 14], 15, -1416354905);
            b = md5_ii(b, c, d, a, x[i + 5], 21, -57434055);
            a = md5_ii(a, b, c, d, x[i + 12], 6, 1700485571);
            d = md5_ii(d, a, b, c, x[i + 3], 10, -1894986606);
            c = md5_ii(c, d, a, b, x[i + 10], 15, -1051523);
            b = md5_ii(b, c, d, a, x[i + 1], 21, -2054922799);
            a = md5_ii(a, b, c, d, x[i + 8], 6, 1873313359);
            d = md5_ii(d, a, b, c, x[i + 15], 10, -30611744);
            c = md5_ii(c, d, a, b, x[i + 6], 15, -1560198380);
            b = md5_ii(b, c, d, a, x[i + 13], 21, 1309151649);
            a = md5_ii(a, b, c, d, x[i + 4], 6, -145523070);
            d = md5_ii(d, a, b, c, x[i + 11], 10, -1120210379);
            c = md5_ii(c, d, a, b, x[i + 2], 15, 718787259);
            b = md5_ii(b, c, d, a, x[i + 9], 21, -343485551);
            a = safe_add(a, olda);
            b = safe_add(b, oldb);
            c = safe_add(c, oldc);
            d = safe_add(d, oldd);
        }
        return [a, b, c, d];
    }
    /*
     * Convert an array of little-endian words to a string
     */
    function binl2rstr(input) {
        var i;
        var output = '';
        for (i = 0; i < input.length * 32; i += 8) {
            output += String.fromCharCode((input[i >> 5] >>> (i % 32)) & 0xFF);
        }
        return output;
    }
    /*
     * Convert a raw string to an array of little-endian words
     * Characters >255 have their high-byte silently ignored.
     */
    function rstr2binl(input) {
        var i;
        var output = [];
        output[(input.length >> 2) - 1] = undefined;
        for (i = 0; i < output.length; i += 1) {
            output[i] = 0;
        }
        for (i = 0; i < input.length * 8; i += 8) {
            output[i >> 5] |= (input.charCodeAt(i / 8) & 0xFF) << (i % 32);
        }
        return output;
    }
    /*
     * Calculate the MD5 of a raw string
     */
    function rstr_md5(s) {
        return binl2rstr(binl_md5(rstr2binl(s), s.length * 8));
    }
    /*
     * Calculate the HMAC-MD5, of a key and some data (raw strings)
     */
    function rstr_hmac_md5(key, data) {
        var i;
        var bkey = rstr2binl(key);
        var ipad = [];
        var opad = [];
        var hash;
        ipad[15] = opad[15] = undefined;
        if (bkey.length > 16) {
            bkey = binl_md5(bkey, key.length * 8);
        }
        for (i = 0; i < 16; i += 1) {
            ipad[i] = bkey[i] ^ 0x36363636;
            opad[i] = bkey[i] ^ 0x5C5C5C5C;
        }
        hash = binl_md5(ipad.concat(rstr2binl(data)), 512 + data.length * 8);
        return binl2rstr(binl_md5(opad.concat(hash), 512 + 128));
    }
    /*
     * Convert a raw string to a hex string
     */
    function rstr2hex(input) {
        var hex_tab = '0123456789abcdef';
        var output = '';
        var x;
        var i;
        for (i = 0; i < input.length; i += 1) {
            x = input.charCodeAt(i);
            output += hex_tab.charAt((x >>> 4) & 0x0F) +
                hex_tab.charAt(x & 0x0F);
        }
        return output;
    }
    /*
     * Encode a string as utf-8
     */
    function str2rstr_utf8(input) {
        //noinspection TypeScriptUnresolvedFunction
        return unescape(encodeURIComponent(input));
    }
    /*
     * Take string arguments and return either raw or hex encoded strings
     */
    function raw_md5(s) {
        return rstr_md5(str2rstr_utf8(s));
    }
    function hex_md5(s) {
        return rstr2hex(raw_md5(s));
    }
    function raw_hmac_md5(k, d) {
        return rstr_hmac_md5(str2rstr_utf8(k), str2rstr_utf8(d));
    }
    function hex_hmac_md5(k, d) {
        return rstr2hex(raw_hmac_md5(k, d));
    }
    function md5(string, key, raw) {
        if (!key) {
            if (!raw) {
                return hex_md5(string);
            }
            return raw_md5(string);
        }
        if (!raw) {
            return hex_hmac_md5(key, string);
        }
        return raw_hmac_md5(key, string);
    }
    //noinspection TypeScriptUnresolvedVariable
    if (typeof define === 'function' && define.amd) {
        //noinspection TypeScriptUnresolvedFunction
        define(function () {
            return md5;
        });
    }
    else {
        if (typeof module === 'object' && module.exports) {
            //noinspection TypeScriptUnresolvedVariable
            module.exports = md5;
        }
        else {
            $.md5 = md5;
        }
    }
}(this));
var Encoding;
(function (Encoding) {
    var MD5 = (function () {
        function MD5() {
        }
        MD5.hash = function (value) {
            if (typeof value !== "string")
                console.log(typeof value);
            //noinspection TypeScriptUnresolvedFunction
            return md5(value);
        };
        return MD5;
    }());
    Encoding.MD5 = MD5;
})(Encoding || (Encoding = {}));
///<reference path="../encoding/Base64.ts"/>
///<reference path="../encoding/MD5.ts"/>
var Middleware;
(function (Middleware) {
    var MD5 = Encoding.MD5;
    var BaseMiddleware = (function () {
        function BaseMiddleware() {
        }
        BaseMiddleware.prototype.handleAsync = function ($next, obj) {
            var _this = this;
            var d = Global.$q.defer();
            if (this.getCache($next)) {
                d.resolve(this.getCache($next));
            }
            else {
                Global.$http.post('/api/handle/' + this.$name + '', obj).success(function (a) {
                    if (a)
                        d.resolve(a);
                    _this.setCache($next, a);
                }).error(function () {
                    _this.setCache($next, {
                        error: true,
                        redirect: '/error'
                    });
                    d.resolve(_this.getCache($next));
                });
            }
            return d.promise;
        };
        BaseMiddleware.prototype.redirect = function (path) {
            Global.$location.path(path);
        };
        BaseMiddleware.prototype.hardRedirect = function (path) {
            //window.location = path;
        };
        BaseMiddleware.prototype.setCache = function (name, value) {
            name = MD5.hash(name);
            BaseMiddleware.cache[this.$name + ':cache:' + name] = value;
        };
        BaseMiddleware.prototype.getCache = function (name) {
            name = MD5.hash(name);
            return BaseMiddleware.cache[this.$name + ':cache:' + name];
        };
        BaseMiddleware.cache = {};
        return BaseMiddleware;
    }());
    Middleware.BaseMiddleware = BaseMiddleware;
})(Middleware || (Middleware = {}));
///<reference path="BaseMiddleware.ts"/>
var Middleware;
(function (Middleware) {
    var Auth = (function (_super) {
        __extends(Auth, _super);
        function Auth() {
            _super.apply(this, arguments);
        }
        /**
         * We handle the middleware here
         * @param $next
         * @returns {string} - this is the url to redirect
         */
        Auth.prototype.handle = function ($next) {
            var user = Authentification.Auth.user();
            if ($next != "/login") {
                if (!user) {
                    Angular.Global.$storage.set('intent', $next);
                    return "#login";
                }
            }
        };
        return Auth;
    }(Middleware.BaseMiddleware));
    Middleware.Auth = Auth;
})(Middleware || (Middleware = {}));
///<reference path="../../app/controllers/BaseController.ts"/>
///<reference path="../../app/User.ts"/>
'use strict';
var Account;
(function (Account) {
    var Controller = Fifth.Controller;
    var AccountController = (function (_super) {
        __extends(AccountController, _super);
        function AccountController() {
            _super.apply(this, arguments);
        }
        AccountController.Index = function ($scope, $rootScope) {
        };
        return AccountController;
    }(Controller));
    Account.AccountController = AccountController;
})(Account || (Account = {}));
///<reference path="../../vendor/fifth/middleware/BaseMiddleware.ts"/>
var Middleware;
(function (Middleware) {
    var HasRole = (function (_super) {
        __extends(HasRole, _super);
        function HasRole(role) {
            _super.call(this);
            this.role = role;
            this.$name = "HasRole";
            return this;
        }
        /**
         * We handle the middleware here
         * @param $next
         * @returns {string} - this is the url to redirect
         */
        HasRole.prototype.handle = function ($next) {
            //these are examples
            var user = Authentification.Auth.user();
            if ($next != "/login" && user) {
                if (this.role instanceof Array) {
                    if (!this.role.some(function (x) { return x == user.role.type; })) {
                        Angular.Global.$storage.set('intent', $next);
                        return "/";
                    }
                }
                else if (user.role.type != this.role) {
                    Angular.Global.$storage.set('intent', $next);
                    return "/";
                }
            }
        };
        return HasRole;
    }(Middleware.BaseMiddleware));
    Middleware.HasRole = HasRole;
})(Middleware || (Middleware = {}));
///<reference path="AccountController.ts"/>
///<reference path="../../app/middleware/HasRole.ts"/>
///<reference path="../../vendor/fifth/middleware/Auth.ts"/>
var Account;
(function (Account) {
    var Auth = Middleware.Auth;
    var HasRole = Middleware.HasRole;
    var Routes = (function () {
        function Routes() {
            Route.Get("/account", {
                uses: Account.AccountController.Index,
                as: 'account',
                view: 'account.index',
                middleware: [new HasRole('patient'), new Auth()]
            });
            Route.Get("/modems", {
                uses: function () { },
                as: 'account.modems',
                view: 'account.modems',
                middleware: []
            });
        }
        return Routes;
    }());
    Account.Routes = Routes;
})(Account || (Account = {}));
/// <reference path="jquery.d.ts" />
/// <reference path="Functions.ts" />
///<reference path="vendor/angular/Global.ts"/>
///<reference path="app/controllers/AuthController.ts"/>
///<reference path="vendor/fifth/middleware/Auth.ts"/>
///<reference path="modules/account/routes.ts"/>
var Route = Fifth.Route;
var Global = Angular.Global;
var AuthController = Fifth.AuthController;
var Auth = Middleware.Auth;
Angular.Global.Init(function () {
    Route.Popup("login", {
        uses: AuthController.Login,
        view: 'auth.login'
    });
    Route.Get("/", {
        uses: function () { },
        as: 'home',
        view: 'account.modems',
        middleware: []
    });
    new Account.Routes();
});
/// <reference path="routes.ts" /> 
//# sourceMappingURL=all.js.map