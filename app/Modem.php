<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modem extends Model
{
    protected $casts = [
        'data' => 'object',
        'connected' => 'boolean'
    ];
}
