<?php

namespace App\Http\Controllers;

use App\Notifications\AccountCreated;
use App\User;
use Illuminate\Http\Request;

class AngularController extends Controller
{
    public function index()
    {

        $user = User::all()->first();
        $user->notify(new AccountCreated());

        return view('welcome');
    }

}
