///<reference path="vendor/angular/Global.ts"/>
///<reference path="app/controllers/AuthController.ts"/>


module Fifth {

    export class Route {
        constructor(public location) {
            return this;
        }

        public static popups = {};
        public static routes = {};

        public static Url(name, params) {
            var route = this.routes[name];
            var d = route;
            for (var i in params) {
                d = d.replace(":" + i, params[i]);
            }
            return d;
        }

        public static Call(url) {
            this.popups[url].init();
        }

        public static Popup(url, options) {
            Route.popups[url] = {
                location: "*",
                init: (a)=> {
                    var dialog = Angular.Global.ngDialog.open({
                        template: options.view,
                        showClose: false,
                        closeByDocument: false,

                        controller: options.uses,
                        resolve: {
                            route: ()=> {
                                return a;
                            }
                        }
                    });
                    dialog.closePromise.then(function (data) {

                        var s = url.split(/(:[a-zA-Z0-9]+)/);

                        var u = Global.$location.hash();
                        console.log(u, url);

                        if (u == url) {
                            Global.$location.hash('');

                        }
                        if (u)
                            return;
                        if (u.indexOf(s[0]) != -1) {
                            Global.$location.hash('');
                        }
                    });
                }
            };
            Route.routes["#" + options.as] = "#" + url;


        }

        public Popup(url, options) {
            Route.popups[url] = {
                location: this.location,
                init: (a)=> {
                    var dialog = Angular.Global.ngDialog.open({
                        template: options.view,
                        showClose: false,
                        closeByDocument: false,
                        controller: options.uses,
                        resolve: {
                            route: ()=> {
                                return a;
                            }
                        }

                    });
                    dialog.closePromise.then(function (data) {
                        var s = url.split(/(:[a-zA-Z0-9]+)/);

                        var u = Global.$location.hash();
                        console.log(u, s[0], u.indexOf(s[0]));
                        if (u)

                            if (u.indexOf(s[0]) != -1) {
                                if (s[0] != "appointment:")
                                    Global.$location.hash('');
                            }

                    });
                }
            };
            Route.routes["#" + options.as] = this.location + "#" + url;


        }

        public static Get(url, options) {
            Angular.Global.app.config(($routeProvider, $locationProvider) => {
                $routeProvider.when(url, {
                    templateUrl: options.view,
                    controller: options.uses,
                    reloadOnSearch: options.reloadOnSearch ? options.reloadOnSearch : false,
                    redirectTo: (a, b, c)=> {
                        if (options.middleware) {
                            var toReturn;
                            for (var i in options.middleware) {
                                toReturn = options.middleware[i].handle(window.location.pathname);
                                if (toReturn)
                                    return toReturn;
                            }

                        }
                    }
                });
                $locationProvider.html5Mode(true);
                $locationProvider.hashPrefix('!');
            });

            if (options.popups) {
                options.popups(new Route(url));
            }
            this.routes[options.as] = url;
        }
    }


}
