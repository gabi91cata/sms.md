///<reference path="../vendor/fifth/models/Model.ts"/>
/**
 * Created by MacBook on 29.08.15.
 */
namespace Fifth{

    import Model = Models.Model;
    export class User extends Model{
        protected $table = "user";
        protected $class = User;
        public name;
        public selected:boolean;
        public id;
        public sip_username;
        public select()
        {
            this.selected = true;
            Global.$location.hash('account:'+this.id);
        }

        public cancel()
        {
            this.selected = false;
            if(this.id=="new")
                this.deleted = true;
            Global.$location.hash('');
        }

        public update($text:string)
        {
            return super.update($text).then(()=>{
                this.selected = false;
                Global.$location.hash('');
            });
        }
    }
}