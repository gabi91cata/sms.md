///<reference path="BaseController.ts"/>
///<reference path="../User.ts"/>
///<reference path="../../vendor/angular/Global.ts"/>
///<reference path="../../vendor/fifth/authentification/Auth.ts"/>

'use strict';

namespace Fifth {

    import Auth = Authentification.Auth;
    export class AuthController extends Controller {

        public static Login($scope, $timeout) {
            $scope.login = new Auth();

        }

        public static Register($scope, $timeout, $location) {
            $scope.register = new Auth();
            $scope.hash = (url)=>{
                $location.hash(url);
            }
        }

        public static Logout($scope, $timeout) {
            Auth.logout();
        }


        public static Recover($scope) {

        }
    }
}
