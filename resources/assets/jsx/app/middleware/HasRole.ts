
///<reference path="../../vendor/fifth/middleware/BaseMiddleware.ts"/>
namespace Middleware {
    export class HasRole extends BaseMiddleware{
        protected $name = "HasRole";


        constructor(private role)
        {
            super();
            return this;
        }
        /**
         * We handle the middleware here
         * @param $next
         * @returns {string} - this is the url to redirect
         */
        public handle($next){
            //these are examples


            var user = Authentification.Auth.user();
            if($next!="/login" && user)
            {

                if(this.role instanceof Array)
                {
                    if(!this.role.some(x=> x==user.role.type))
                    {
                        Angular.Global.$storage.set('intent', $next);
                        return "/";
                    }
                }
                else
                if(user.role.type != this.role)
                {

                    Angular.Global.$storage.set('intent', $next);
                    return "/";
                }
            }
        }
    }

}
