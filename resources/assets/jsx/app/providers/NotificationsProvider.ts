
/**
 * Created by MacBook on 29.08.15.
 */
namespace Providers{

    export class NotificationsProvider {
        private ws;
        constructor(public $scope?, public Websocket?, public $mdToast?, public $location?){
            this.init();
            return this;
        }

        private init()
        {

            var toastShown = false;

            this.Websocket.onMessage([
                "App\\Notifications\\AppointmentReminder",
                "App\\Notifications\\AppointmentBooked",
                "App\\Notifications\\AppointmentReview",
                "App\\Notifications\\AppointmentCanceled"
            ], (message)=>{
                if(toastShown)
                {
                    this.$mdToast.updateTextContent(message.text);
                    return;
                }

                var toast = this.$mdToast.simple()
                    .textContent(message.text)
                    .action('Deschide')
                    .hideDelay(16000)
                    .highlightClass('md-accent')// Accent is used by default, this just demonstrates the usage.
                    .position('top right');
                toastShown = true;
                this.$mdToast.show(toast).then((response) => {
                    if ( response == 'ok' ) {
                        this.$location.path(message.action)
                        this.$location.search('');
                    }
                    toastShown = false;
                });

            });
        }
    }
}