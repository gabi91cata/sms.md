/**
 * Created by MacBook on 30.08.15.
 */
/// <reference path="jquery.d.ts" />

module Fifth{
    export interface IUploadSong{
        button:JQuery;
        preview:[JQuery];
        file:JQuery;
        success:Function;
    }

    export interface IResizeImage{
        button:JQuery;
        aspectRatio:number;
        preview:{
            blur: JQuery;
            original:JQuery
        }
    }
}