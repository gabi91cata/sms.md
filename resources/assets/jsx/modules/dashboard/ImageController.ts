///<reference path="../../app/controllers/BaseController.ts"/>
'use strict';


namespace Dashboard{

    import Dialog = Angular.Dialog;
    import Controller = Fifth.Controller;
    export class ImageController extends Controller{


        public static Profile($scope, $rootScope,$timeout, $routeParams, fgDelegate, route)
        {
            $scope.loading = true;
            var cropper;

            var zoom = 1;
            setTimeout(()=>{
                cropper = $('.image-editor').cropit({
                    exportZoom: 1.25,
                    imageBackground: true,

                });

                $(".cropit-image-input").change(()=>{
                    $scope.loading = false;
                    $scope.$apply();
                    $(this).val('');
                })


                $('.rotate-cw').click(function() {
                    $('.image-editor').cropit('rotateCW');
                });

                $('.load-image').click(function() {
                    $(".cropit-image-input").trigger('click');
                    $scope.loading = true;

                    $scope.$apply();
                });
                $('.rotate-ccw').click(function() {
                    $('.image-editor').cropit('rotateCCW');
                });

                  $(".cropit-image-input").trigger('click');


            }, 150)


            $scope.save = ()=>{
                route.model($('.image-editor').cropit('export'));
                $scope.closeThisDialog();
            }
        }


    }
}
