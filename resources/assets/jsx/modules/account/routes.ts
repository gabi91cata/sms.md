///<reference path="AccountController.ts"/>
///<reference path="../../app/middleware/HasRole.ts"/>
///<reference path="../../vendor/fifth/middleware/Auth.ts"/>

namespace Account{


    import Auth = Middleware.Auth;
    import HasRole = Middleware.HasRole;
    export class Routes {

        constructor()
        {

            Route.Get("/account",{
                uses: AccountController.Index,
                as:'account',
                view: 'account.index',
                middleware: [new HasRole('patient'), new Auth()]
            } );

            Route.Get("/modems",{
                uses: ()=>{},
                as:'account.modems',
                view: 'account.modems',
                middleware: []
            } );


        }

    }
}
