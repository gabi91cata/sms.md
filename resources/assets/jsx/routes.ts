/// <reference path="jquery.d.ts" />
/// <reference path="Functions.ts" />
///<reference path="vendor/angular/Global.ts"/>
///<reference path="app/controllers/AuthController.ts"/>
///<reference path="vendor/fifth/middleware/Auth.ts"/>
///<reference path="modules/account/routes.ts"/>

import Route = Fifth.Route;
import Global = Angular.Global;
import AuthController = Fifth.AuthController;
import Auth = Middleware.Auth;


Angular.Global.Init(()=> {
    Route.Popup("login",{
        uses: AuthController.Login,
        view: 'auth.login'
    });

    Route.Get("/",{
        uses: ()=>{},
        as:'home',
        view: 'account.modems',
        middleware: [  ]
    } );
    new Account.Routes();
});
