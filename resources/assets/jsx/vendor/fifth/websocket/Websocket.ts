///<reference path="../../angular/Global.ts"/>
///<reference path="../authentification/Auth.ts"/>



namespace Socket {

    import Global = Angular.Global;
    import Auth = Authentification.Auth;
    export class Websocket {

        private ws;
        constructor(public localStorageService){
            this.init();
            Auth.on('login', (user) => {
                this.init();

            });
            Auth.on('logout', (user) => {
                this.init();
            });

            return this;
        }

        public publish(type:string, object:any){
            object['type'] = type;
            if(this.ws.readyState == 1)
                this.ws.send(JSON.stringify(object));
            else
                this.ws.addEventListener('open', () => {
                    this.ws.send(JSON.stringify(object));
                });
        }
        private callbacks = {};

        public onMessage(type:any,callback:any){

            if(type instanceof Array)
            {
                for(var t of type)
                {
                    if(!this.callbacks[t])
                        this.callbacks[t] = [];
                    this.callbacks[t].push(callback);
                }
            }
            else
            {
                if(!this.callbacks[type])
                    this.callbacks[type] = [];
                this.callbacks[type].push(callback);
            }

        }
        private init(){

            var user = this.localStorageService.get('auth:user');
            if(!user)
                user = {password:'guest'};

                if(this.ws)
                    this.ws.close();
            this.ws = new WebSocket(Config.app.ws+"?token="+user.sip_password  );
            this.ws.addEventListener('message', (data:MessageEvent)=>{
                var o = JSON.parse(data.data);
                var object = o.data;
                for(var type in this.callbacks)
                {
                    if(type == object.type || type == '*')
                    {
                        for(var j in this.callbacks[type])
                        {
                            this.callbacks[type][j](object);
                        }
                    }
                }
                Angular.Global.$scope.$apply();
            });
            this.ws.addEventListener('open', () => {
                this.ws.send(JSON.stringify({subscribe:'App.User.'+user.id}));
            });
            this.ws.addEventListener('close', ()=>{
            });
            this.ws.addEventListener('error', ()=>{
            });




        }
    }
}