///<reference path="../../angular/Global.ts"/>
///<reference path="../../../config/App.ts"/>


namespace Ajax {

    export var Get = function(url:string){
        return Angular.Global.$http.get(Config.app.api+url);
    };


    export var Post = function(url:string, data:any){
        data['_token'] = window['CSRF_TOKEN'];
        console.log(window);
        return Angular.Global.$http.post(Config.app.api+url, data);
    };

    export var Delete = function(url:string){
        return Angular.Global.$http.delete(Config.app.api+url);
    };
}