///<reference path="../../angular/Global.ts"/>



namespace Imager {

    import Global = Angular.Global;
    export class CropImage {

        private featherEditor;
        constructor(){
            this.init();

            return this;
        }

        public profile(src){
            window['featherEditor'].launch({
                image: 'test',
                tools: 'enhance,effects,orientation,crop,lighting,color,sharpness,focus,vignette,text',
                url: 'http://consultadoctor.ro/resize.php?zc=1&h=450&w=750&src=/images/spuneNu.jpg',
                onSave: (imageID, newURL) => {
                    console.log(newURL);
                    this.featherEditor.close();
                }
            });
        }
        private init(){


        }
    }
}