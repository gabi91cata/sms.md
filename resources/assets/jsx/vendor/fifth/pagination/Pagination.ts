///<reference path="../../angular/Global.ts"/>
/**
 * Created by MacBook on 29.08.15.
 */

namespace Pagination {

    import Global = Angular.Global;
    export class Paginator {


        /**
         * Paginate the results
         * @returns any
         * @param val
         */
        public static paginate(val:any) {
            try {
                var pages = [];
                var begin = 1;
                var end = val.last_page;
                if (val.last_page > 5) {
                    if (val.current_page > 3)
                        begin = val.current_page - 2;
                    if (val.last_page - val.current_page > 3)
                        end = val.current_page + 3;
                }
                for (var i = begin; i <= end; i++) {
                    pages.push({
                        page: i,
                        active: val.current_page == i,
                        prev: val.current_page > 1,
                        next: val.current_page < val.last_page
                    });
                }
                val.pages = pages;
            }
            catch(e){
                console.error(e);
            }
            return val;
        }
    }
}