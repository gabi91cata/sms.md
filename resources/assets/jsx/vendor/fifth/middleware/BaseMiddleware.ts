///<reference path="../encoding/Base64.ts"/>
///<reference path="../encoding/MD5.ts"/>

namespace Middleware {
    import Base64 = Encoding.Base64;
    import MD5 = Encoding.MD5;
    export class BaseMiddleware {

        protected $name;
        protected $next;

        public handleAsync($next, obj){
            var d = Global.$q.defer();
            if(this.getCache($next))
            {
                d.resolve(this.getCache($next));
            }
            else
            {
                Global.$http.post('/api/handle/'+this.$name+'', obj).success((a)=>{
                    if(a)
                        d.resolve(a);
                    this.setCache($next, a);
                }).error(()=>{
                    this.setCache($next, {
                        error: true,
                        redirect: '/error'
                    });
                    d.resolve(this.getCache($next));

                });
            }

            return d.promise;

        }

        public redirect(path)
        {
            Global.$location.path(path);
        }

        public hardRedirect(path)
        {
            //window.location = path;
        }

        static cache = {};
        private setCache(name, value){
            name = MD5.hash(name);
            BaseMiddleware.cache[this.$name+':cache:'+name] = value
        }
        private getCache(name){
            name = MD5.hash(name);
            return BaseMiddleware.cache[this.$name+':cache:'+name];
        }
    }

}