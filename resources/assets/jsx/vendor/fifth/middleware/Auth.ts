 ///<reference path="BaseMiddleware.ts"/>


namespace Middleware {
    export class Auth extends BaseMiddleware{

        /**
         * We handle the middleware here
         * @param $next
         * @returns {string} - this is the url to redirect
         */
        public handle($next){

            var user = Authentification.Auth.user();
            if($next!="/login")
            {
                if(!user)
                {
                    Angular.Global.$storage.set('intent', $next);
                    return "#login";
                }
            }

        }
    }

}
