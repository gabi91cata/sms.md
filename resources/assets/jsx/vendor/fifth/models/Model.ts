
///<reference path="../../angular/Global.ts"/>
///<reference path="../pagination/Pagination.ts"/>
///<reference path="BaseModel.ts"/>
///<reference path="../encoding/Base64.ts"/>
/**
 * Created by MacBook on 29.08.15.
 */

namespace Models{

    import IHttpPromise = angular.IHttpPromise;
    import Paginator = Pagination.Paginator;
    import User = Fifth.User;
    import Base64 = Encoding.Base64;

    export class Model extends BaseModel{
        protected  $table;
        protected  $class = Model;
        protected $dates = [];
        protected $with = {};
        protected $errors = {};
        protected $deleteText;

        protected  $cache:boolean = false;
        private saving = false;
        private deleting = false;
        private confirming = false;
        public deleted = false;
        public loading = false;
        private attributes:any = {};
        private wheres = [];
        public listAll = [];
        private data = {};
        constructor(attributes={}){
            super();
            this.attributes = attributes;
            for(var i in attributes)
            {
                this[i] = attributes[i];
            }
            delete(this.attributes);
            return this;
        }

        protected getDeletedText()
        {
            return "Datele au fost sterse cu succes!";
        }
        protected getSavedText()
        {
            return "Datele au fost salvate cu succes!";
        }

        protected getDenyText()
        {
            return "Nu ai drepturi necesare!";
        }

        /**
         * Get the model by id
         * @param id
         * @returns {Models.Model}
         */
        public find(id:number)
        {
            var where = "";
            if(this.data)
                where += '&data='+Base64.encode(JSON.stringify(this.data));


            this.loading = true;


            Global.$http.get('/api/'+this.$table+'/'+id+"?"+where).success((val:any)=>{
                this.loading = false;
                this.constructor(val);
                this.resolveDates();

                if(this._then)
                    this._then(this)


                    for(var l of this._thens)
                    {
                        l.cb(val);
                    }

            }).error((e)=>{
                console.log(e);
                this.loading = false;
            });
            return this;
        }

        private _then;
        private _thens = [];
        public later(val)
        {
            this._then = val;
            return this;
        }
        public laters(val)
        {
            this._thens.push({cb:val});  ;

            return this;
        }

        public reload(attributes)
        {
            this.attributes = attributes;
            for(var i in attributes)
            {
                this[i] = attributes[i];
            }
            delete(this.attributes);
            this.resolveDates();

        }
        /**
         * Delete current model
         * @returns {IHttpPromise<T>}
         */
        public remove($deleteText = null) {

            if($deleteText)
            {
                this.deleting = true;

                Global.Dialog.confirm("Confirma stergerea", $deleteText, null, null).then(()=>{

                    Global.$http.delete('/api/'+this.$table+'/' + this['id']).success((val:any)=>{
                        this.clearCache();
                        Global.$mdToast.show(Global.$mdToast.simple().textContent(this.getDeletedText()));

                        this.constructor(val);
                        this.resolveDates();
                        this.deleting = false;
                        this.deleted = true;
                    }).error((errors, a)=>{

                        this.deleted = false;

                        this.deleting = false;
                        if(a==403)
                        {
                            Global.Dialog.errors({email:[this.getDenyText()]}, 10000);
                        }
                        else
                        {
                            Global.Dialog.errors(errors, 10000);
                        }

                    });;
                },()=>{
                    this.deleting = false;
                    this.deleted = false;

                });

            }
            else
            {
                this.deleting = true;
                var d = Global.$q.defer();
                Global.$http.delete('/api/'+this.$table+'/' + this['id']).success((val:any)=>{
                    this.clearCache();
                    d.resolve();
                    this.deleting = false;
                    this.deleted = true;
                });
                return  d.promise;
            }

        }
        public getAll()
        {
            return this.listAll;
        }
        public save($text = null)
        {
            if(this['id'] && this['id'] != 'new')
                return this.update($text);

            var d = Global.$q.defer();
            this.saving = true;
            this['_token'] = window['CSRF_TOKEN'];

            var where = "";
            if(this.data)
                where += '&data='+Base64.encode(JSON.stringify(this.data));

            Global.$http.post('/api/'+this.$table+"?"+where, this).success((val:any)=>{
                this.clearCache();
                d.resolve((new this.$class(val)).resolveDates());
                this.reload(val);
                this.saving = false;
                Global.$mdToast.show(Global.$mdToast.simple().position('bottom right').textContent($text?$text:this.getSavedText()));
            }).error((errors,a)=>{
                this.saving = false;
                d.reject(errors);
                this.$errors = errors;

                if(a==403)
                {
                    Global.Dialog.errors({email:[this.getDenyText()]}, 10000);
                }
                else
                {
                    Global.Dialog.errors(errors, 10000);
                }


            });
            return  d.promise;
        }

        public update($text:string):any
        {
            if(!this['id'] || this['id'] == 'new')
                return this.save($text);
            var d = Global.$q.defer();
            this.saving = true;
            var where = "";
            if(this.data)
                where += '&data='+Base64.encode(JSON.stringify(this.data));

            Global.$http.put('/api/'+this.$table+'/'+this['id']+"?"+where, this).success((val:any)=>{
                this.clearCache();
                this.constructor(val);
                d.resolve(new this.$class(val).resolveDates());
                this.saving = false;
                Global.$mdToast.show(Global.$mdToast.simple().position('bottom right').textContent($text?$text:this.getSavedText()));
            }).error((errors, a)=>{
                this.saving = false;
                d.reject(errors);
                this.$errors = errors;

                if(a==403)
                {
                    Global.Dialog.errors({email:[this.getDenyText()]}, 10000);

                }
                else
                {
                    Global.Dialog.errors(errors, 10000);
                }


            });

            return  d.promise;
        }


        public with(data){
            this.data = data;
            return this;
        }
        public where(...args:any[]) {
            switch(args.length)
            {
                case 1:
                    throw Error('Must have at least 2 arguments');
                case 2:
                    this.wheres.push({
                        key: args[0],
                        condition: '=',
                        value: args[1]
                    });
                    break;
                case 3:
                    this.wheres.push({
                        key: args[0],
                        condition: args[1],
                        value: args[2]
                    });
                    break;

            }
            return this;
        }

        private getCache(name)
        {
            if(!this.$cache)
                return false;
            var storage = Global.$storage;
            name = Base64.encode(name);
            var n = storage.get(this.$table+':cache:'+name);
            if(n)
                return true;
            return false;
        }

        private setCache(name, val)
        {
            if(!this.$cache)
                return false;
            var storage = Global.$storage;
            name = Base64.encode(name);
            var n = storage.set(this.$table+':cache:'+name, val);
        }
        private caching(name){
            var storage = Global.$storage;
            name = Base64.encode(name);
            var n =  storage.get(this.$table+':cache:'+name);
            if(n.data)
            {
                for(var i in n.data)
                {
                    n.data[i] =  (new this.$class(n.data[i])).resolveDates();
                }
            }
            var d = Global.$q.defer();
            d.resolve(n);
            return d.promise;
        }


        private clearCache()
        {
            var keys = (Global.$storage.keys());
            for(var i in keys)
            {
                if(i.indexOf(this.$table+':cache:')!=-1)
                Global.$storage.remove(i);
            }
        }


        /**
         * Get all the models here
         * @param page
         * @returns {IPromise<T>}
         */
        public get(page=null) {

            var storage = Global.$storage;
            if (!page)
                page = storage.get(this.$table+':page');
            storage.set(this.$table+':page', page);
            var d = Global.$q.defer();
            if(page != null)
                page = 'page='+page;
            else
                page = '';
            var where = '';
            this.loading = true;
            if(this.wheres.length > 0)
                where = '&where='+Base64.encode(JSON.stringify(this.wheres));
            if(this.data)
                where += '&data='+Base64.encode(JSON.stringify(this.data));
            if(this.getCache(page+where))
            {
                return this.caching(page+where);
            }
            Global.$http.get('/api/'+this.$table+'?' + page+where).then((response:any)=> {
                var val = Paginator.paginate(response.data);

                var loop = [];
                if(val.data)
                    loop = val.data;
                else
                    loop = val


                if(loop[0])
                {
                    for(var i in loop)
                        loop[i] =  (new this.$class(loop[i])).resolveDates();
                }


                for(var j in this.$with)
                {

                    if(val[j] && !val[j].data)
                    {
                        if( Object.prototype.toString.call( val[j] ) === '[object Array]' )
                            for(var i in val[j])
                                val[j][i] =  (new this.$with[j](val[j][i])).resolveDates();
                        else
                            val[j] =  (new this.$with[j](val[j])).resolveDates();
                    }

                    if(val[j] && val[j].data)
                    {
                        if( Object.prototype.toString.call( val[j].data ) === '[object Array]' )
                            for(var i in val[j].data)
                                val[j].data[i] =  (new this.$with[j](val[j].data[i])).resolveDates();


                    }
                }



                this.listAll = val;
                this.setCache(page+where, val);
                if(val.data)
                {
                    val.data = loop;
                    d.resolve(val);
                }
                else
                    d.resolve(loop);
                this.loading = false;


            });
            return d.promise;
        }

        public resolveDates()
        {
            var dates = this.$dates;
            dates.push('created_at');
            dates.push('updated_at');
            for(var i in this)
            {
                if(dates.indexOf(i) != -1)
                {
                    if(this[i])
                    this[i] = new Date(this[i]);
                }
            }


            for(var j in this.$with)
            {

                if(this[j])
                {
                    if( Object.prototype.toString.call( this[j] ) === '[object Array]' )
                        for(var i in this[j])
                            this[j][i] =  (new this.$with[j](this[j][i])).resolveDates();
                    else
                        this[j] =  (new this.$with[j](this[j])).resolveDates();


                    }
            }

            return this;
        }
    }

}