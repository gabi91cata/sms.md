///<reference path="../../angular/Global.ts"/>
///<reference path="../ajax/Ajax.ts"/>
///<reference path="../../../routes.ts"/>


namespace Authentification {

    import Get = Ajax.Get;
    import Post = Ajax.Post;
    export class Auth {
        private email;
        private password ;
        private loading = false;
        private errors = {};
        constructor() {
            return this;
        }

        private static cbs = {};
        public static on(type:string, callback:any)
        {
            if(!Auth.cbs[type])
                Auth.cbs[type] = [];

            Auth.cbs[type].push(callback);
        }
        /**
         * Here we grab the user and see if it's logged
         * @returns {JQueryXHR|*|ICacheObject|T|IHttpPromise<T>|any}
         */
        public static user() {
            if(window['user_json'])
                return window['user_json'];
            return Angular.Global.$storage.get('auth:user');
        }

        public static watch(cb)
        {
            Angular.Global.$scope.$watch(function(){
                return Auth.user();
            }, cb, true)
        }

        /**
         * Here will logout the application
         */
        public static logout()
        {
            Get('Logout').success(()=>{
                Angular.Global.$storage.set('auth:user', null);
                   window['user_json'] = null;
                for(var i in Auth.cbs['logout'])
                    Auth.cbs['logout'][i]();
                Global.$location.hash('');
                Global.$location.path('/');
            });
        }

        public register(type)
        {
            this['type'] = type;
            this.loading = true;

            Post('Register', this).success((result:any)=> {
                this.loading = false;
                Global.$location.hash('');
                Global.$mdToast.show(Global.$mdToast.simple().position('top right').textContent("Contul a fost creat. Te rugam sa verifici si sa confirmi adresa de email."));
                Global.Dialog.swal('swals.register', result);

            }).error((errors)=>{
                Global.Dialog.errors(errors);
                this.loading = false;

            });
        }
        /**
         * Here we submit the data to the server to verify it
         */
        public submit() {
            this.loading = true;
            this.errors = [];
            Post('Login', this).success((result:any)=>{
                var user = result.user;

                if(!user)
                {
                    this.errors = result.errors;
                    this.loading = false;
                    return;
                }
                user.token = result.token;
                Angular.Global.$storage.set('auth:user', user);

                for(var i in Auth.cbs['login'])
                    Auth.cbs['login'][i](user);
                var intent = Angular.Global.$storage.get('intent');
                Angular.Global.$storage.remove('intent');
                if(intent)
                    window.location.assign(intent);
                else
                {
                    Global.$location.hash('');
                }
            }).error((errors)=>{
                console.log(errors);
                Global.Dialog.errors(errors);
                this.loading = false;

            });;
        }
    }
}