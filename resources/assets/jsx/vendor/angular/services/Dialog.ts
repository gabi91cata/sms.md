///<reference path="../../../modules/dashboard/ImageController.ts"/>


namespace Angular {
    import ImageController = Dashboard.ImageController;
    export class Dialog {
        constructor() {
            Angular.Global.app.service('Dialog', function ($mdDialog, ngDialog) {


                this.changeProfile = (cb, image = null)=>{
                    var dialog = ngDialog.open({
                        template: 'dash.image.profile',
                        showClose:false,
                        controller: ImageController.Profile,
                        resolve:{
                            route: function(){
                                return {
                                    model: cb,
                                    images: image
                                }
                            }
                        }
                    });
                }

                this.confirm = (title:string, text:string, target:any, parent:any, ok:string = "Da", cancel:string = "Renunta")=>{
                    var confirm = $mdDialog.confirm()
                        .title(title)
                        .textContent(text)
                        .clickOutsideToClose(true)
                        .parent(angular.element($(parent)))
                        .targetEvent(target)
                        .ok(ok)
                        .cancel(cancel);
                    return $mdDialog.show(confirm)
                };
                this.alert = (title:string, text:string, target:any, parent:any, ok:string = "OK")=>{
                    var confirm = $mdDialog.alert()
                        .title(title)
                        .textContent(text)
                        .clickOutsideToClose(true)
                        .parent(angular.element($(parent)))
                        .targetEvent(target)
                        .ok(ok)
                    return $mdDialog.show(confirm)
                };


                this.errors = (errors)=>{
                    var messages = "";
                    console.error(errors);

                    for(var error in errors)
                    {
                        for(var e of errors[error])
                            messages+= e+"<br/>";
                    }

                    Global.$mdToast.show({
                        hideDelay   : 6000,
                        position    : 'top right',
                        controller  : ()=>{},
                        template : `<md-toast style="z-index: 99999999;" class="error-toast">
  <span  >`+messages+`</span> 
</md-toast>`
                    });
                };

                this.show = (view, controller, data, ev )=>{
                    return $mdDialog.show({
                        controller: controller,
                        templateUrl: view,
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        locals : data
                    }) ;
                };

                this.swal = (view, object)=>{
                    var dialog = ngDialog.open({
                        template: view,
                        showClose:false,
                        controller: ($scope, $location)=>{
                            $scope.object = object;
                            $scope.open = ()=>{

                            }
                        }

                    });
                };

                return this;
            });
        }
    }
}