///<reference path="directives/Loading.ts"/>
///<reference path="directives/Container.ts"/>
///<reference path="directives/Pagination.ts"/>
///<reference path="directives/Input.ts"/>
///<reference path="directives/Flip.ts"/>
///<reference path="services/Dialog.ts"/>
///<reference path="../fifth/websocket/Websocket.ts"/>
///<reference path="Global.ts"/>
///<reference path="../fifth/image/Crop.ts"/>
///<reference path="directives/Animation.ts"/>
///<reference path="directives/Rating.ts"/>
/**
 * Created by MacBook on 13.02.2016.
 */

namespace Angular {
    import Websocket = Socket.Websocket;
    import CropImage = Imager.CropImage;
    export class Services {
        constructor() {
            ///Services
            new Dialog();
            Angular.Global.app.service('Websocket', Websocket);
            ///Directives
            new Loading();
            new Rating();
            new Flip();
            new Container();
            new Pagination();
            new Input();
            Angular.Global.app.directive('ngAnimate', Animation);

        }
    }
}