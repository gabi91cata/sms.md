///<reference path="../../Functions.ts"/>
///<reference path="Services.ts"/>
///<reference path="../fifth/ajax/Ajax.ts"/>
///<reference path="../fifth/authentification/Auth.ts"/>
///<reference path="services/Dialog.ts"/>
///<reference path="../../modules/dashboard/ImageController.ts"/>
///<reference path="../../app/providers/NotificationsProvider.ts"/>

namespace Angular{
    import IHttpService = angular.IHttpService;
    import IQService = angular.IQService;
    import IScope = angular.IScope;
    import Get = Ajax.Get;
    import Auth = Authentification.Auth;
    import ILocationProvider = angular.ILocationProvider;
    import ImageController = Dashboard.ImageController;
    import NotificationsProvider = Providers.NotificationsProvider;
    export class Global{
        public static app:any;
        public static vars = {};
        public static $http:IHttpService;
        public static localStorageService;
        public static $storage;
        public static $location;
        public static $route;
        public static $mdToast;
        public static opened;
        public static ngDialog;
        public static Dialog;
        public static Websocket;
        public static $q:IQService;
        public static $scope:any;

        public static meta(title)
        {
            document.title = title;
        }
        private static Load()
        {
            var c;



        }

        public static Init(cb){


            Date.prototype.toJSON = function(a)
            {
                var d = new Date(this.valueOf() - this.getTimezoneOffset() * 60000);;
                return d.toISOString();
            }
            this.app = angular.module('fifth', [    'ngRoute', 'LocalStorageModule',  'ngDialog', 'ngMaterial']);

            $("body").on("mouseover", "[md-selectable]",  function (e) {
               e.preventDefault();
                e.stopPropagation();
                $(this).parent().parent().addClass('md-non-selectable');
                console.log($(this).parent().parent());
            });
            this.app.directive('mdSelectable', function($parse){
                return {
                    restrict: 'A',
                    link($scope, $elem, iAttrs){


                    }
                };
            }).filter('cut', function () {
                return function (value, wordwise, max, tail) {
                    if (!value) return '';

                    max = parseInt(max, 10);
                    if (!max) return value;
                    if (value.length <= max) return value;

                    value = value.substr(0, max);
                    if (wordwise) {
                        var lastspace = value.lastIndexOf(' ');
                        if (lastspace != -1) {
                            //Also remove . and , so its gives a cleaner result.
                            if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
                                lastspace = lastspace - 1;
                            }
                            value = value.substr(0, lastspace);
                        }
                    }

                    return value + (tail || ' …');
                };
            });
            this.app.filter('nl2br', function($sce){
                return function(msg,is_xhtml) {
                    var is_xhtml = is_xhtml || true;
                    var breakTag = (is_xhtml) ? '<br />' : '<br>';
                    var msg:any = (msg + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
                    msg = msg.replace(/  /g, '&nbsp; ');
                    return $sce.trustAsHtml(msg);
                }
            });

            this.app.filter('data', function($sce){
                return function(msg,is_xhtml) {

                    return new Date(msg);
                }
            });
            this.app.directive('endRepeat', function() {
                return {
                    controller: ($scope)=>{
                            if ($scope.$last){
                                window.alert("im the last!");
                            }

                    }
                }
            });
            this.app.filter('contains', function() {
                return function (array, needle) {
                    return array.indexOf(needle) >= 0;
                };
            });
            this.app.directive("myStream", function($window){
                return {
                    restrict: 'A',
                    scope:{config:'='},
                    link: function(scope, element, attributes){

                    }
                }

            })
            this.app.config(function($mdThemingProvider, $sceDelegateProvider) {
                $sceDelegateProvider.resourceUrlWhitelist([
                    'self',                    // trust all resources from the same origin
                    '*://www.youtube.com/**',  // trust all resources from `www.youtube.com`
                    '*:blob:https://adoclive.com/**'  // this might be how to test from mediaStream.
                ]);



                $mdThemingProvider.theme('default')
                    .primaryPalette('teal')
                    .accentPalette('teal');

                $mdThemingProvider.theme('black')
                    .primaryPalette('teal')
                    .accentPalette('teal');


                $mdThemingProvider.theme('200')
                    .primaryPalette('teal')
                    .accentPalette('teal');


            });


            this.app.controller('MainController', ($scope, Websocket, Dialog, $routeParams, ngDialog, $route, $rootScope, $timeout, $location, $http,$mdToast, $q, localStorageService) =>{
                localStorageService.set('auth:user', window['user_json']);

                $http.defaults.headers.common['X-CSRF-TOKEN']= window['CSRF_TOKEN'];
                $http.defaults.headers.common['X-Requested-With']= 'XMLHttpRequest';

                $scope.side = false;
                $rootScope.route = $routeParams;
                $scope.loaded = true;
                $rootScope.ip = window['ip'];
                Global.$http = $http;
                Global.Websocket = Websocket;
                Global.$route = $route;
                Global.$storage = localStorageService;
                Global.$q = $q;
                Global.$location = $location;
                Global.ngDialog = ngDialog;
                Global.$mdToast = $mdToast;
                Global.$scope = $scope;
                Global.Dialog = Dialog;
                $scope.logout = function(){
                    Auth.logout();
                };
                $scope.hash = function(a){
                    $location.hash(a);
                };

                function extract(a, sp, r = null) {
                    if(r==null)
                        r = [];
                    var regex = /(:[a-zA-Z0-9]+)/;
                    var b:any = [];
                    for (var i in sp) {
                        var item = sp[i];
                        if (item != "") {
                            if(a)
                                b = a.split(item);

                            if(b.length == 2)
                                if(b[0] == "" && b[1] == "")
                                    return true;
                            if(b[1])
                            {
                                //we found a match, so we continue
                                r.push(sp[1]);

                                if(sp[0] && sp[1])
                                    sp.splice(0,2);
                                if(sp.length == 1)
                                {
                                    var ret = {};
                                    for(var j in b){
                                        if(b[j] == "")
                                        {
                                            b.splice(j,1);
                                        }
                                    }
                                    if(r[0])
                                    {
                                        for(var j in b)
                                        {
                                            ret[r[j].replace(':', '')] = b[j];
                                        }
                                    }
                                    if(!jQuery.isEmptyObject(ret))
                                        return ret;
                                }
                                return extract(b[1], sp, r);
                            }
                        }
                    }
                    return false;
                }
                $scope.$watch(()=>{return $location.hash()}, (a)=>{

                    ngDialog.close();

                    if(a)
                    {
                        var r = Route.popups[a];
                        $timeout(()=>{

                            for (var route in Route.popups) {
                                var s = route.split(/(:[a-zA-Z0-9]+)/);
                                var sp = extract(a, s);
                                if(sp)
                                {
                                    var loc = (Global.$route.current.$$route.originalPath);
                                    var r = Route.popups[route];

                                    if(r.location == loc || r.location == "*")
                                    {
                                        r.init(sp);
                                    }
                                }
                            }
                        },1);
                    }
                })


            });

            cb();


            this.app.directive('ngName', function($location) {
                    return {
                        scope:{
                            data:'=ngData'
                        },
                        link:  function (scope, element, attrs) {
                            var route = Fifth.Route.routes[attrs.ngName];
                            for(var i in scope.data)
                            {
                                route = route.replace(":"+i,scope.data[i]);
                            }

                            if(Global.$location.path() == route)
                                $(element).addClass('active');

                            $(element).attr('href', route);
                            if(scope.data)
                                if(scope.data.type)
                                    $(element).attr('href', route+"?type="+ scope.data.type);

                            $(element).click(function(){
                                $("[ng-name]").removeClass('active');
                                $(element).addClass('active');
                                Global.$location.hash('');
                                if(!$(element).is("a"))
                                {
                                    Global.$location.path(route);
                                    if(scope.data)
                                        if(!scope.data.type)
                                            Global.$location.search('');
                                        else
                                        {
                                            Global.$location.search("type", scope.data.type);
                                        }
                                }

                                console.log(scope.data);
                                Global.$scope.$apply();
                            });
                        }
                    };
                });


            this.app.config(function (localStorageServiceProvider) {
                localStorageServiceProvider
                    .setPrefix('myApp')
                    .setStorageType('sessionStorage')
                    .setNotify(true, true)
            });

            new Services();

        }
    }
}