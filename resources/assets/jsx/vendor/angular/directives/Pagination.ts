/**
 * Created by MacBook on 13.02.2016.
 */

namespace Angular {
    export class Pagination {
        constructor() {
            Angular.Global.app.directive('ngPagination', function () {
                return {
                    restrict: 'E',
                    scope:{
                        callback:'=ngCallback',
                        data: '=ngData'
                    },
                    template:`
                    <ul class="pagination" ng-if="data.pages.length > 1">
                        <li><a ng-click="callback(1)" href="">«</a></li>
                        <li ng-repeat="page in data.pages" ng-class="{active:page.active}"><a ng-click="callback(page.page)" href="" ng-bind="page.page"></a></li>
                        <li><a ng-click="callback(data.last_page)" href="">»</a></li>
                    </ul>
                    `
                };
            });
        }
    }
}