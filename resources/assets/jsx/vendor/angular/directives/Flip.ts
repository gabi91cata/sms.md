/**
 * Created by MacBook on 13.02.2016.
 */

namespace Angular {
    export class Flip {
        constructor() {
            Angular.Global.app.directive('ngFlip', function () {
                return {
                    restrict: 'A',
                    transclude: true,
                    scope: {
                        cond: '=ngFlip',
                        class: '@'
                    },
                    link: function(scope, element, attr){
                        $(element).removeClass(scope.class);
                    },
                    template: `
                    <div class="flip-container {{ class }}" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper">
                            <div class="front">
                                 <div ng-transclude></div>
                            </div>
                            <div class="back md-whiteframe-12dp" ng-if="cond" >
                               <div class="cssload-container" >
                            <table>
                                <tr>
                                    <td>
                                     <div layout="row" layout-sm="column" layout-align="space-around">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div>

                                    </td>
                                    </tr>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>`

                };
            });
        }
    }
}