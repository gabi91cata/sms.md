/**
 * Created by MacBook on 13.02.2016.
 */

namespace Angular {
    export class Input {
        constructor() {
            Angular.Global.app.directive('ngInput', function () {
                return {
                    restrict: 'E',
                    scope:{
                        type:'@',
                        name: '@',
                        label: '@',
                        ngModel:'='
                    },
                    transclude:true,
                    template:`
                    <div class="form-group label-floating" ng-transclude="">
                        <label class="control-label" for="{{name}}">{{label}}</label>
                        <input class="form-control" id="{{name}}" type="{{type}}" autocomplete="false"  ng-model="ngModel" >
                    </div>
                    `
                };
            });
        }
    }
}