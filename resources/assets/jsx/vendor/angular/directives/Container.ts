/**
 * Created by MacBook on 13.02.2016.
 */

namespace Angular {
    export class Container {
        constructor() {
            Angular.Global.app.directive('container', function () {
                return {
                    restrict: 'E',
                    scope:{
                        col:'=',
                        align:'@'
                    },
                    transclude: true,
                    template: `
                        <div class="container">
                            <div class="row">
                                <div class="{{ class }}" ng-transclude>
                                </div>
                            </div>
                        </div>`,

                    link: function(scope){
                        if(scope.align == 'c' || !scope.align)
                        {
                            scope.class = 'col-md-'+scope.col;
                            var offset = (12-scope.col)/2;
                            scope.class += ' col-md-offset-'+offset;
                        }
                        if(scope.align == 'r')
                        {
                            scope.class = 'col-md-'+scope.col;
                            var offset = (12-scope.col);
                            scope.class += ' col-md-offset-'+offset;
                        }

                        if(scope.align == 'l')
                        {
                            scope.class = 'col-md-'+scope.col;
                        }
                    }
                };
            });
        }
    }
}