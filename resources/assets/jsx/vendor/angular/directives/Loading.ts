/**
 * Created by MacBook on 13.02.2016.
 */

namespace Angular {
    export class Loading {
        constructor() {
            Angular.Global.app.directive('ngLoading', function () {
                return {
                    restrict: 'A',
                    transclude: true,
                    scope: {
                        cond: '=ngLoading',
                        class: '@'
                    },
                    link: function(scope, element, attr){
                        $(element).removeClass(scope.class);
                    },
                    template: `
                    <div class="relative {{ class }}" >
                        <div ng-transclude></div>
                        <div class="cssload-container" ng-if="cond">
                            
                            <md-progress-linear md-mode="indeterminate"  ></md-progress-linear> 
                            </div>
                    </div>`

                };
            });
            Angular.Global.app.directive('ngInvalid', function () {
                return {
                    restrict: 'A',
                    transclude: true,
                    scope: {
                        cond: '=ngInvalid',
                        class: '@'
                    },
                    link: function(scope, element, attr){
                        $(element).removeClass(scope.class);
                    },
                    template: `
                    <div class="relative {{ class }}" >
                        <div ng-transclude></div>
                        <div class="cssload-container" ng-if="cond">
                            
                            
                            </div>
                    </div>`

                };
            });
        }
    }
}