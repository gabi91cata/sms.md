/**
 * Created by MacBook on 13.02.2016.
 */

namespace Angular {
    export class Rating {
        constructor() {
            Angular.Global.app.directive('ngRating', function () {
                return {
                    restrict: 'E',
                    scope: {
                        stars: '=stars',
                    },
                    link: function(scope, element, attr){
                        $(element).removeClass(scope.class);
                        scope.integer = Math.round(scope.stars);
                        scope.decimal = scope.integer - scope.stars;
                        scope.decimal = Math.round(Math.abs(scope.decimal) * 10);
                    },
                    template: `   
                        <md-icon class="star md-primary" ng-repeat="item in [1,2,3,4,5]" ng-if="item<=stars" >star</md-icon>
                        <md-icon class="star md-primary" ng-repeat="item in [1,2,3,4,5]" ng-if="item>stars" >star_border</md-icon>
                    
                     `

                };
            });
        }
    }
}