namespace Angular {
    export class Animation {
        restrict = 'A';
        link = (scope, element, attrs) => {
          var delay = 0;
          if(attrs.delay)
          {
              delay = attrs.delay;
              $(element).addClass('hide');

          }

            setTimeout(()=>{
              $(element).removeClass('hide');
              $(element).addClass('animated '+attrs.ngAnimate);

            }, delay);


        }
        constructor(){
            return this;
        }

    }
}
