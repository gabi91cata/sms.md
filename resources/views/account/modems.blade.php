<div class="modems">
    <div class="sky-bg md-padding">
        <div class="row">
            <div class="col-md-7">
                <h3>Conectare modem</h3>
                <p>Conectearea modemurilor se face doar folosind software-ul oferit de noi.</p>
            </div>
            <div class="col-md-5">
                <div class="bg-white md-padding rounded">
                    Descarca de aici software-ul si introdu cheia de licenta:
                    <div>HJ@_#DJSA#@! DSA #@</div>
                    <div class="text-center">
                        <md-button class="md-primary">
                            Descarca software
                        </md-button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>