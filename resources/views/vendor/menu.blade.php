<div md-colors="{color:'teal-50'}" >

    <div>
        <div class="md-padding">
            <h4>
                <md-icon md-colors="{color:'green'}">fiber_manual_record</md-icon>
               test
            </h4>
        </div>
    </div>
    <md-list class="md-dense"  >
        <md-list-item ng-name="dashboard" ng-click="1">
            <md-icon md-colors="{color:'teal-200'}">dashboard</md-icon>
            <p>Panoul de control</p>
        </md-list-item>
        <md-list-item ng-name="dashboard.account">
            <md-icon md-colors="{color:'purple-200'}">assignment_ind</md-icon>
            <p>Contul meu</p>
        </md-list-item>

        <md-divider></md-divider>
        <md-list-item ng-name="account.modems"  >
            <md-icon md-colors="{color:'blue-200'}">usb</md-icon>
            <p>Modem-uri SMS  </p>
        </md-list-item>

        <md-list-item ng-name="account.campaigns">
            <md-icon md-colors="{color:'black-200'}">compare_arrows</md-icon>
            <p>Campanii</p>
        </md-list-item>
        <md-divider></md-divider>
        <md-list-item ng-name="dashboard.payments">
            <md-icon md-colors="{color:'purple-200'}">trending_up</md-icon>
            <p>Plati</p>
        </md-list-item>
        <md-list-item ng-name="dashboard.invoices">
            <md-icon md-colors="{color:'orange-200'}">assignment</md-icon>
            <p>Facturi</p>
        </md-list-item>
        <md-list-item ng-name="dashboard.invoicing">
            <md-icon md-colors="{color:'red-200'}">assignment_turned_in</md-icon>
            <p>Setari facturi</p>
        </md-list-item>


    </md-list>

</div>
