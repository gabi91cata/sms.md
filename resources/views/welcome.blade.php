<!DOCTYPE html>
<html lang="ro_RO" ng-app="fifth" ng-cloak>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon"
          type="image/png"
          href="{{ url("/assets/img/logo-icon-50.png") }}">

    <title>SMS</title>

    <base href="/">
    <!-- Fonts -->
    <link href="//fonts.googleapis.com/icon?family=Material+Icons|Slabo+27px|Ubuntu:400,300" rel="stylesheet">
    <!-- Styles -->

    <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/bower_components/angular-material/angular-material.min.css" rel="stylesheet">
    <link href="/bower_components/angular-material/angular-material.min.css" rel="stylesheet">
    <link href="/bower_components/angular-editer/stylesheets/simditor.css" rel="stylesheet">
    <link href="/bower_components/angular-editer/stylesheets/font-awesome.css" rel="stylesheet">
    <link href="/bower_components/material-photo-gallery/dist/css/material-photo-gallery.css" rel="stylesheet">

    <link href="/assets/css/app.min.css" rel="stylesheet">
    <link href="/bower_components/ng-dialog/css/ngDialog.min.css" rel="stylesheet">

</head>
<body  ng-controller="MainController"  >

        <div layout="row" class="main-wrapper" layout-align="left none" ng-cloak>
            <div>
                @include('vendor.menu')
            </div>
            <section  flex  class="main-content"   md-whiteframe="5">
                @include('vendor.header')
                <div>
                    <div ng-view></div>
                </div>
            </section>
        </div>

@foreach($templates as $template)
    <script type="text/ng-template" id="{{  $template}}">
    @include($template)
    </script>
@endforeach

<script type="text/javascript">
    window['CSRF_TOKEN'] = '{{ csrf_token() }}';
</script>

<!-- JavaScripts -->
<script src="https://maps.google.com/maps/api/js?libraries=placeses,visualization,drawing,geometry,places&key=AIzaSyBCts_ol0j6c9WJ9U2om1GjhNCm-m-wyhI"></script>
<script src="/bower_components/angular/angular.min.js"></script>
<script src="/bower_components/angular-route/angular-route.min.js"></script>
<script src="/bower_components/angular-local-storage/dist/angular-local-storage.min.js"></script>
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/moment/min/moment.min.js"></script>
<script src="/bower_components/moment/min/locales.min.js"></script>
<script src="/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="/bower_components/fullcalendar/dist/lang/ro.js"></script>
<script src="/bower_components/angular-ui-calendar/src/calendar.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/snackbarjs/dist/snackbar.min.js"></script>
<script src="/bower_components/angular-animate/angular-animate.min.js"></script>
<script src="/bower_components/angular-aria/angular-aria.min.js"></script>
<script src="/bower_components/angular-messages/angular-messages.min.js"></script>
<script src="/bower_components/angular-material/angular-material.min.js"></script>
<script src="/bower_components/ngmap/build/scripts/ng-map.min.js"></script>
<script src="/bower_components/angular-i18n/angular-locale_ro.js"></script>
<script src="/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script src="/bower_components/angular-editer/javascripts/simditor/simditor-all.js"></script>
<script src="/bower_components/angular-editer/javascripts/angular-editor.js"></script>
<script src="/bower_components/angular-sortable-view/src/angular-sortable-view.min.js"></script>
<script src="/bower_components/ngFlowGrid/src/ngFlowGrid.js"></script>
<script src="/bower_components/md-steppers/dist/md-steppers.min.js"></script>
<script src='/bower_components/textAngular/dist/textAngular-rangy.min.js'></script>
<script src='/bower_components/textAngular/dist/textAngular-sanitize.min.js'></script>
<script src='/bower_components/textAngular/dist/textAngular.min.js'></script>
<script src="/bower_components/angular-aviary/angular-aviary.js"></script>
<script src="/bower_components/cropit/dist/jquery.cropit.js"></script>
<script src="/bower_components/ng-flow/dist/ng-flow-standalone.min.js"></script>
<script src="/bower_components/humanize-duration/humanize-duration.js"></script>
<script src="/bower_components/angular-timer/dist/angular-timer.min.js"></script>
<script src="/bower_components/jssip/dist/jssip.min.js"></script>
<script src="/bower_components/angular-timeago/dist/angular-timeago.min.js"></script>
<script src="/bower_components/material-photo-gallery/dist/js/material-photo-gallery.js"></script>
<script src="/bower_components/angular-timeago/src/languages/time-ago-language-ro_RO.js"></script>
<script src="/bower_components/ng-dialog/js/ngDialog.min.js"></script>
<!-- Angular Material Library -->

<script src="/assets/js/all.js"></script>

</body>
</html>
