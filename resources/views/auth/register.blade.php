<container col="6" class="panel-body">


    <md-card class="md-whiteframe-12dp" md-watch-theme  ng-loading="register.loading">
        <div md-colors="{background:'default-primary'}" class="md-padding text-center">
            <md-icon style="font-size: 50px; width: 50px; height: 50px; color: white; margin: 7px;">person_add</md-icon>
            <h3 class="no-margin">Inregistreaza-te !</h3>

        </div>

        <div class="md-padding" layout="row">

            <div flex>
                <h4>
                    <md-icon>person</md-icon> Pacient
                </h4>
                <p>
                    Creeaza un cont de pacient acum si programeaza-te direct in agenda medicului sau pentru un serviciu oferit prin videoconferinta
                </p>
                <md-button ng-click="hash('register_patient')" class="md-raised md-warn">Sunt pacient</md-button>
            </div>
            <div flex>
                <h4>
                    <md-icon>local_hospital</md-icon> Furnizor servicii medicale
                </h4>
                <p>
                    Intra in reteaua <strong>ConsultaDoctor</strong> si cu siguranta iti vei creste numarul de pacienti fie in cabinet fie prin videoconferinta
                </p>
                <md-button ng-click="hash('register_doctor')" class="md-raised md-primary">Vreau sa apar pe site</md-button>

            </div>
        </div>
    </md-card>
</container>
